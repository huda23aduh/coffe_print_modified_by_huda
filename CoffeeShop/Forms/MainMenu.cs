﻿using CoffeeShop.Properties;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Windows.Forms;
using AForge.Video.DirectShow;
using Goheer.EXIF;

namespace CoffeeShop.Forms
{
    public partial class MainMenu : Form
    {
        public bool painted;

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        public Settings Pengaturan = new Settings();

        public MainMenu()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Init();         
        }

        public void Init()
        {
            RunWebServer();
            ReadInitialSettings();
        }

        public void RunWebServer()
        {
            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                FileName = "run.bat",
                Arguments = "",
                WindowStyle = ProcessWindowStyle.Minimized
            };
            Process.Start(startInfo);
        }

        public void ReadInitialSettings()
        {
            string[] separator = new string[] { "\r\n" };
            string[] strArray = File.ReadAllText("CoffeeShop.ini").Split(separator, StringSplitOptions.RemoveEmptyEntries);

            var i = strArray[18].IndexOf('=');
            var va = strArray[18].Substring(i + 1, strArray[18].Length - (i + 1));

            if (strArray.Length != 20)
            {
                MessageBox.Show("Aplikasi error. Silahkan install ulang aplikasi.");
                ProperExit();
            }
            else if (!(va == MainMenu.GetHdDid()))
            {
                MessageBox.Show("Silahkan hubungi customer service kami untuk aktivasi.", "Aplikasi tidak boleh dicopy.");
                ProperExit();
                Environment.Exit(0);
            }
            else
            {
                foreach (var str in strArray)
                {
                    var index = str.IndexOf('=');
                    var propName = str.Substring(0, index);
                    var value = str.Substring(index + 1, str.Length - (index + 1));
                    PropertySetLooping(Pengaturan, propName, value);
                }
            }
        }

        public static string GetHdDid()
        {
            ManagementObject dsk = new ManagementObject(@"win32_logicaldisk.deviceid=""c:""");
            dsk.Get();
            string id = dsk["VolumeSerialNumber"].ToString();
            return id;
        }

        public static string GetLocalIpAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        //public static string GetVideoDevice()
        //{
        //    num = 0;
        //    FilterInfoCollection info = new FilterInfoCollection(FilterCategory.VideoInputDevice);
        //    foreach (info in this.videoDevices)
        //    {
        //        this.comboBox2.Items.Add(info.Name);
        //        if (info.Name == this.cm3)
        //        {
        //            this.comboBox2.SelectedIndex = num;
        //        }
        //        num++;
        //    }
        //    this.comboBox2.SelectedIndex = 0;
        //}

        public void ProperExit()
        {
            foreach (var process in Process.GetProcessesByName("WebDev.WebServer40"))
            {
                process.Kill();
            }
            Application.Exit();
        }

        private static void PropertySetLooping(object p, string propName, object value)
        {
            Type t = p.GetType();
            foreach (PropertyInfo info in t.GetProperties())
            {
                if (info.Name == propName && info.CanWrite)
                {
                    if (info.Name.Equals("NetWorkName"))
                    {
                        var ipAdd = Convert.ChangeType(GetLocalIpAddress(), info.PropertyType);
                        info.SetValue(p, ipAdd, null);
                    }
                    else
                    {
                        var changedObj = Convert.ChangeType(value, info.PropertyType);
                        info.SetValue(p, changedObj, null);
                        break;
                    }
                }
            }
        }

        public void SetAllControlsResourcesToDefault()
        {
            Upload.BackgroundImage = Resources.Selfie;
            Template.BackgroundImage = Resources.Template;
            Settings.BackgroundImage = Resources.Settings;
        }

        // Upload Button

        private void Upload_Click(object sender, EventArgs e)
        {
            //to uploadform
            //this.Hide();
            //var upload = new UploadForm(this, Pengaturan);
            //upload.Show();

            //to new_form_upload
            this.Hide();
            NewFormUpload new_form_upload = new NewFormUpload(this, Pengaturan);
            new_form_upload.Show();
        }

        private void Upload_MouseEnter(object sender, EventArgs e)
        {
            Upload.BackgroundImage = Resources.Selfie_SelfieClicked;
            Upload.ForeColor = Color.White;
        }

        private void Upload_MouseLeave(object sender, EventArgs e)
        {
            Upload.BackgroundImage = Resources.Selfie;
        }

        // Template Button

        private void Template_Click(object sender, EventArgs e)
        {
            this.Hide();
            var template = new TemplateForm(this, Pengaturan);
            template.Show();
        }

        private void Template_MouseEnter(object sender, EventArgs e)
        {
            Template.BackgroundImage = Resources.Template_TemplateClicked;
        }

        private void Template_MouseLeave(object sender, EventArgs e)
        {
            Template.BackgroundImage = Resources.Template;
        }

        // Settings Button

        private void Settings_Click(object sender, EventArgs e)
        {
            this.Hide();
            var settings = new SettingsForm(this, Pengaturan);
            settings.Show();
        }

        private void Settings_MouseEnter(object sender, EventArgs e)
        {
            Settings.BackgroundImage = Resources.SettingsClicked;
        }

        private void Settings_MouseLeave(object sender, EventArgs e)
        {
            Settings.BackgroundImage = Resources.Settings;
        }

        // Minimize Button

        private void Mini_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void Mini_MouseEnter(object sender, EventArgs e)
        {
            Mini.BackgroundImage = Resources.MiniClicked;
        }

        private void Mini_MouseLeave(object sender, EventArgs e)
        {
            Mini.BackgroundImage = Resources.Mini;
        }

        // Exit Button

        private void Exit_Click(object sender, EventArgs e)
        {
            Exit.BackgroundImage = Resources.ExitClicked;
            DialogResult prompt = MessageBox.Show("Apakah yakin anda ingin keluar?", "Keluar Program", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Warning);
            if (prompt == DialogResult.OK)
            {
                ProperExit();
            }
            else
            {
                Exit.BackgroundImage = Resources.Exit;
            }
        }

        private void Exit_MouseEnter(object sender, EventArgs e)
        {
            Exit.BackgroundImage = Resources.ExitClicked;
        }

        private void Exit_MouseLeave(object sender, EventArgs e)
        {
            Exit.BackgroundImage = Resources.Exit;
        }

        private void MainMenu_VisibleChanged(object sender, EventArgs e)
        {
            if (Pengaturan.Startup.Equals("Selfie Only"))
            {
                Upload.Visible = true;
                Upload.Enabled = true;
                Template.Visible = false;
                Template.Enabled = false;
                Upload.Anchor = (AnchorStyles.Left | AnchorStyles.Right);
            }
            else if (Pengaturan.Startup.Equals("Template Only"))
            {
                Template.Visible = true;
                Template.Enabled = true;
                Upload.Visible = false;
                Upload.Enabled = false;
                Template.Anchor = (AnchorStyles.Left | AnchorStyles.Right);
            }
            else
            {
                Upload.Visible = true;
                Upload.Enabled = true;
                Upload.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
                Template.Visible = true;
                Template.Enabled = true;
                Template.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
            }
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        // move to new form huda
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            NewFormUpload new_form_upload = new NewFormUpload(this, Pengaturan);
            new_form_upload.Show();
        }
    }
}