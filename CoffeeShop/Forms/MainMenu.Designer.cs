﻿namespace CoffeeShop.Forms
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Exit = new System.Windows.Forms.Button();
            this.Mini = new System.Windows.Forms.Button();
            this.Settings = new System.Windows.Forms.Button();
            this.Logo = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Footer = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Template = new System.Windows.Forms.PictureBox();
            this.Upload = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Template)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Upload)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel1.BackgroundImage")));
            this.tableLayoutPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.Logo, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.20261F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 64.51776F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.27964F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1280, 788);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 4);
            this.panel1.Controls.Add(this.Exit);
            this.panel1.Controls.Add(this.Mini);
            this.panel1.Controls.Add(this.Settings);
            this.panel1.Location = new System.Drawing.Point(712, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(565, 138);
            this.panel1.TabIndex = 6;
            // 
            // Exit
            // 
            this.Exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Exit.BackColor = System.Drawing.Color.Transparent;
            this.Exit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Exit.BackgroundImage")));
            this.Exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exit.FlatAppearance.BorderSize = 0;
            this.Exit.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Exit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Exit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exit.Location = new System.Drawing.Point(472, 18);
            this.Exit.Margin = new System.Windows.Forms.Padding(3, 25, 5, 3);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(82, 83);
            this.Exit.TabIndex = 4;
            this.Exit.UseVisualStyleBackColor = false;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            this.Exit.MouseEnter += new System.EventHandler(this.Exit_MouseEnter);
            this.Exit.MouseLeave += new System.EventHandler(this.Exit_MouseLeave);
            // 
            // Mini
            // 
            this.Mini.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Mini.BackColor = System.Drawing.Color.Transparent;
            this.Mini.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Mini.BackgroundImage")));
            this.Mini.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Mini.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Mini.FlatAppearance.BorderSize = 0;
            this.Mini.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Mini.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Mini.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Mini.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mini.Location = new System.Drawing.Point(398, 18);
            this.Mini.Margin = new System.Windows.Forms.Padding(3, 25, 5, 3);
            this.Mini.Name = "Mini";
            this.Mini.Size = new System.Drawing.Size(76, 82);
            this.Mini.TabIndex = 5;
            this.Mini.UseVisualStyleBackColor = false;
            this.Mini.Click += new System.EventHandler(this.Mini_Click);
            this.Mini.MouseEnter += new System.EventHandler(this.Mini_MouseEnter);
            this.Mini.MouseLeave += new System.EventHandler(this.Mini_MouseLeave);
            // 
            // Settings
            // 
            this.Settings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Settings.BackColor = System.Drawing.Color.Transparent;
            this.Settings.BackgroundImage = global::CoffeeShop.Properties.Resources.Settings;
            this.Settings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Settings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Settings.FlatAppearance.BorderSize = 0;
            this.Settings.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Settings.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Settings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Settings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Settings.Location = new System.Drawing.Point(324, 18);
            this.Settings.Margin = new System.Windows.Forms.Padding(3, 25, 5, 3);
            this.Settings.Name = "Settings";
            this.Settings.Size = new System.Drawing.Size(77, 82);
            this.Settings.TabIndex = 0;
            this.Settings.UseVisualStyleBackColor = false;
            this.Settings.Click += new System.EventHandler(this.Settings_Click);
            this.Settings.MouseEnter += new System.EventHandler(this.Settings_MouseEnter);
            this.Settings.MouseLeave += new System.EventHandler(this.Settings_MouseLeave);
            // 
            // Logo
            // 
            this.Logo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Logo.BackColor = System.Drawing.Color.Transparent;
            this.Logo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Logo.BackgroundImage")));
            this.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tableLayoutPanel1.SetColumnSpan(this.Logo, 2);
            this.Logo.Enabled = false;
            this.Logo.FlatAppearance.BorderSize = 0;
            this.Logo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Logo.Location = new System.Drawing.Point(28, 19);
            this.Logo.Margin = new System.Windows.Forms.Padding(28, 16, 3, 3);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(147, 165);
            this.Logo.TabIndex = 3;
            this.Logo.UseVisualStyleBackColor = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.panel2, 8);
            this.panel2.Controls.Add(this.Footer);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 701);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1274, 84);
            this.panel2.TabIndex = 7;
            // 
            // Footer
            // 
            this.Footer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Footer.BackgroundImage")));
            this.Footer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Footer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Footer.Enabled = false;
            this.Footer.FlatAppearance.BorderSize = 0;
            this.Footer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Footer.Location = new System.Drawing.Point(0, 0);
            this.Footer.Name = "Footer";
            this.Footer.Size = new System.Drawing.Size(1274, 84);
            this.Footer.TabIndex = 0;
            this.Footer.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.panel3, 8);
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Controls.Add(this.Template);
            this.panel3.Controls.Add(this.Upload);
            this.panel3.Location = new System.Drawing.Point(3, 193);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1274, 502);
            this.panel3.TabIndex = 11;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::CoffeeShop.Properties.Resources.Selfie;
            this.pictureBox1.Location = new System.Drawing.Point(764, 25);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(268, 305);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Template
            // 
            this.Template.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Template.BackColor = System.Drawing.Color.Transparent;
            this.Template.BackgroundImage = global::CoffeeShop.Properties.Resources.Template;
            this.Template.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Template.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Template.Location = new System.Drawing.Point(595, 25);
            this.Template.Name = "Template";
            this.Template.Size = new System.Drawing.Size(268, 305);
            this.Template.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.Template.TabIndex = 10;
            this.Template.TabStop = false;
            this.Template.Click += new System.EventHandler(this.Template_Click);
            this.Template.MouseEnter += new System.EventHandler(this.Template_MouseEnter);
            this.Template.MouseLeave += new System.EventHandler(this.Template_MouseLeave);
            // 
            // Upload
            // 
            this.Upload.BackColor = System.Drawing.Color.Transparent;
            this.Upload.BackgroundImage = global::CoffeeShop.Properties.Resources.Selfie;
            this.Upload.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Upload.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Upload.Location = new System.Drawing.Point(306, 25);
            this.Upload.Name = "Upload";
            this.Upload.Size = new System.Drawing.Size(268, 305);
            this.Upload.TabIndex = 9;
            this.Upload.TabStop = false;
            this.Upload.Click += new System.EventHandler(this.Upload_Click);
            this.Upload.MouseEnter += new System.EventHandler(this.Upload_MouseEnter);
            this.Upload.MouseLeave += new System.EventHandler(this.Upload_MouseLeave);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 788);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CoffeeShop";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.VisibleChanged += new System.EventHandler(this.MainMenu_VisibleChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Template)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Upload)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button Settings;
        private System.Windows.Forms.Button Logo;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Button Mini;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox Upload;
        private System.Windows.Forms.PictureBox Template;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button Footer;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

