﻿namespace CoffeeShop.Forms
{
    partial class UploadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UploadForm));
            this.Zoom = new System.Windows.Forms.Button();
            this.Title = new System.Windows.Forms.PictureBox();
            this.Home = new System.Windows.Forms.Button();
            this.Print = new System.Windows.Forms.Button();
            this.Delete = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Logo = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.TimerUploadToShow = new System.Windows.Forms.Timer(this.components);
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Mini = new System.Windows.Forms.Button();
            this.Exit = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Insert = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btn_cold = new System.Windows.Forms.Button();
            this.btn_hot = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // Zoom
            // 
            this.Zoom.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Zoom.BackColor = System.Drawing.Color.Transparent;
            this.Zoom.BackgroundImage = global::CoffeeShop.Properties.Resources.Zoom_Move_Selfie_mac;
            this.Zoom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Zoom.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Zoom.FlatAppearance.BorderSize = 0;
            this.Zoom.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Zoom.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Zoom.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Zoom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Zoom.ForeColor = System.Drawing.Color.Transparent;
            this.Zoom.Location = new System.Drawing.Point(422, 32);
            this.Zoom.Name = "Zoom";
            this.Zoom.Size = new System.Drawing.Size(394, 57);
            this.Zoom.TabIndex = 21;
            this.Zoom.UseVisualStyleBackColor = false;
            this.Zoom.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Zoom_MouseDown);
            this.Zoom.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Zoom_MouseMove);
            // 
            // Title
            // 
            this.Title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Title.BackColor = System.Drawing.Color.Transparent;
            this.Title.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Title.BackgroundImage")));
            this.Title.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Title.Location = new System.Drawing.Point(585, 35);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(150, 50);
            this.Title.TabIndex = 25;
            this.Title.TabStop = false;
            // 
            // Home
            // 
            this.Home.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Home.BackColor = System.Drawing.Color.Transparent;
            this.Home.BackgroundImage = global::CoffeeShop.Properties.Resources.Selfie_Home;
            this.Home.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Home.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Home.FlatAppearance.BorderSize = 0;
            this.Home.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Home.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Home.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Home.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Home.ForeColor = System.Drawing.Color.Transparent;
            this.Home.Location = new System.Drawing.Point(93, 15);
            this.Home.Margin = new System.Windows.Forms.Padding(25, 3, 3, 3);
            this.Home.Name = "Home";
            this.Home.Size = new System.Drawing.Size(88, 90);
            this.Home.TabIndex = 24;
            this.Home.UseVisualStyleBackColor = false;
            this.Home.Click += new System.EventHandler(this.Home_Click);
            this.Home.MouseEnter += new System.EventHandler(this.Home_MouseEnter);
            this.Home.MouseLeave += new System.EventHandler(this.Home_MouseLeave);
            // 
            // Print
            // 
            this.Print.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Print.BackColor = System.Drawing.Color.Transparent;
            this.Print.BackgroundImage = global::CoffeeShop.Properties.Resources.Selfie_Print;
            this.Print.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Print.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Print.FlatAppearance.BorderSize = 0;
            this.Print.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Print.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Print.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Print.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Print.Location = new System.Drawing.Point(1122, 17);
            this.Print.Margin = new System.Windows.Forms.Padding(25, 3, 3, 3);
            this.Print.Name = "Print";
            this.Print.Size = new System.Drawing.Size(88, 90);
            this.Print.TabIndex = 22;
            this.Print.UseVisualStyleBackColor = false;
            this.Print.Click += new System.EventHandler(this.Print_Click);
            this.Print.MouseEnter += new System.EventHandler(this.Print_MouseEnter);
            this.Print.MouseLeave += new System.EventHandler(this.Print_MouseLeave);
            // 
            // Delete
            // 
            this.Delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Delete.BackColor = System.Drawing.Color.Transparent;
            this.Delete.BackgroundImage = global::CoffeeShop.Properties.Resources.Selfie_Delete;
            this.Delete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Delete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Delete.FlatAppearance.BorderSize = 0;
            this.Delete.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Delete.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Delete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Delete.Location = new System.Drawing.Point(996, 19);
            this.Delete.Margin = new System.Windows.Forms.Padding(25, 3, 3, 3);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(88, 90);
            this.Delete.TabIndex = 23;
            this.Delete.UseVisualStyleBackColor = false;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            this.Delete.MouseEnter += new System.EventHandler(this.Delete_MouseEnter);
            this.Delete.MouseLeave += new System.EventHandler(this.Delete_MouseLeave);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Location = new System.Drawing.Point(394, 172);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(484, 477);
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            // 
            // Logo
            // 
            this.Logo.BackColor = System.Drawing.Color.Transparent;
            this.Logo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Logo.BackgroundImage")));
            this.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Logo.Location = new System.Drawing.Point(34, 32);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(147, 170);
            this.Logo.TabIndex = 26;
            this.Logo.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Location = new System.Drawing.Point(962, 195);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(292, 242);
            this.pictureBox2.TabIndex = 27;
            this.pictureBox2.TabStop = false;
            // 
            // TimerUploadToShow
            // 
            this.TimerUploadToShow.Enabled = true;
            this.TimerUploadToShow.Interval = 500;
            this.TimerUploadToShow.Tick += new System.EventHandler(this.TimerUploadToShow_Tick);
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.Mini);
            this.panel3.Controls.Add(this.Exit);
            this.panel3.Location = new System.Drawing.Point(988, 12);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(258, 157);
            this.panel3.TabIndex = 28;
            // 
            // Mini
            // 
            this.Mini.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Mini.BackgroundImage = global::CoffeeShop.Properties.Resources.Mini;
            this.Mini.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Mini.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Mini.FlatAppearance.BorderSize = 0;
            this.Mini.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Mini.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Mini.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Mini.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mini.Location = new System.Drawing.Point(98, 17);
            this.Mini.Name = "Mini";
            this.Mini.Size = new System.Drawing.Size(76, 82);
            this.Mini.TabIndex = 5;
            this.Mini.UseVisualStyleBackColor = true;
            this.Mini.Click += new System.EventHandler(this.Mini_Click);
            this.Mini.MouseEnter += new System.EventHandler(this.Mini_MouseEnter);
            this.Mini.MouseLeave += new System.EventHandler(this.Mini_MouseLeave);
            // 
            // Exit
            // 
            this.Exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Exit.BackgroundImage = global::CoffeeShop.Properties.Resources.Exit;
            this.Exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exit.FlatAppearance.BorderSize = 0;
            this.Exit.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Exit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Exit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exit.Location = new System.Drawing.Point(172, 17);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(82, 83);
            this.Exit.TabIndex = 4;
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            this.Exit.MouseEnter += new System.EventHandler(this.Exit_MouseEnter);
            this.Exit.MouseLeave += new System.EventHandler(this.Exit_MouseLeave);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(175, 373);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "label1";
            this.label1.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.Home);
            this.panel1.Controls.Add(this.Insert);
            this.panel1.Controls.Add(this.Print);
            this.panel1.Controls.Add(this.Zoom);
            this.panel1.Controls.Add(this.Delete);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 662);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1280, 126);
            this.panel1.TabIndex = 30;
            // 
            // Insert
            // 
            this.Insert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Insert.BackColor = System.Drawing.Color.Transparent;
            this.Insert.BackgroundImage = global::CoffeeShop.Properties.Resources.Selfie_Insert;
            this.Insert.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Insert.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Insert.FlatAppearance.BorderSize = 0;
            this.Insert.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Insert.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Insert.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Insert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Insert.ForeColor = System.Drawing.Color.Transparent;
            this.Insert.Location = new System.Drawing.Point(216, 15);
            this.Insert.Margin = new System.Windows.Forms.Padding(25, 3, 3, 3);
            this.Insert.Name = "Insert";
            this.Insert.Size = new System.Drawing.Size(88, 90);
            this.Insert.TabIndex = 31;
            this.Insert.UseVisualStyleBackColor = false;
            this.Insert.Click += new System.EventHandler(this.Insert_Click);
            this.Insert.MouseEnter += new System.EventHandler(this.Insert_MouseEnter);
            this.Insert.MouseLeave += new System.EventHandler(this.Insert_MouseLeave);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BackgroundImage = global::CoffeeShop.Properties.Resources.watermark_atas;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Enabled = false;
            this.pictureBox3.Location = new System.Drawing.Point(394, 182);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(484, 473);
            this.pictureBox3.TabIndex = 32;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // btn_cold
            // 
            this.btn_cold.BackgroundImage = global::CoffeeShop.Properties.Resources.cold_coffe_logo1;
            this.btn_cold.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cold.Location = new System.Drawing.Point(169, 450);
            this.btn_cold.Name = "btn_cold";
            this.btn_cold.Size = new System.Drawing.Size(75, 86);
            this.btn_cold.TabIndex = 34;
            this.btn_cold.UseVisualStyleBackColor = true;
            this.btn_cold.Click += new System.EventHandler(this.btn_cold_Click);
            // 
            // btn_hot
            // 
            this.btn_hot.BackColor = System.Drawing.Color.Red;
            this.btn_hot.BackgroundImage = global::CoffeeShop.Properties.Resources.hot_coffe_logo;
            this.btn_hot.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_hot.Location = new System.Drawing.Point(88, 450);
            this.btn_hot.Name = "btn_hot";
            this.btn_hot.Size = new System.Drawing.Size(75, 86);
            this.btn_hot.TabIndex = 35;
            this.btn_hot.UseVisualStyleBackColor = false;
            this.btn_hot.Click += new System.EventHandler(this.btn_hot_Click);
            // 
            // UploadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1280, 788);
            this.Controls.Add(this.btn_hot);
            this.Controls.Add(this.btn_cold);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.Logo);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "UploadForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UploadForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.UploadForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button Zoom;
        private System.Windows.Forms.Button Print;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.Button Home;
        private System.Windows.Forms.PictureBox Title;
        private System.Windows.Forms.PictureBox Logo;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Timer TimerUploadToShow;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button Mini;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button Insert;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button btn_cold;
        private System.Windows.Forms.Button btn_hot;
    }
}