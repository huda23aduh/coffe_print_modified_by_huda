﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using CoffeeShop.Properties;
using static System.String;
using System.Printing.Interop;

namespace CoffeeShop.Forms
{
    public partial class SettingsForm : Form
    {
        public MainMenu _menuUtama;
        private Settings _pengaturan;
        private string filename;

        private OpenFileDialog choofdlog = new OpenFileDialog
        {
            Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.jpg;*.bmp;*.png;*.gif)|*.BMP;*.JPG;*.GIF;*.jpg;*.bmp;*.png;*.gif",
            FilterIndex = 1,
            Multiselect = false
        };

        [DllImport("winspool.Drv", EntryPoint = "DocumentPropertiesW", SetLastError = true,
            ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        private static extern int DocumentProperties(IntPtr hwnd, IntPtr hPrinter,
            [MarshalAs(UnmanagedType.LPWStr)] string pDeviceName,
            IntPtr pDevModeOutput, ref IntPtr pDevModeInput, int fMode);

        [DllImport("kernel32.dll")]
        private static extern IntPtr GlobalLock(IntPtr hMem);

        [DllImport("kernel32.dll")]
        private static extern bool GlobalUnlock(IntPtr hMem);

        [DllImport("kernel32.dll")]
        private static extern bool GlobalFree(IntPtr hMem);

        [DllImport("winspool.Drv", EntryPoint = "DocumentPropertiesW", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        private static extern int DocumentProperties(IntPtr hwnd, IntPtr hPrinter, [MarshalAs(UnmanagedType.LPWStr)] string pDeviceName, IntPtr pDevModeOutput, IntPtr pDevModeInput, int fMode);

        private const int DM_PROMPT = 4;
        private const int DM_OUT_BUFFER = 2;
        private const int DM_IN_BUFFER = 8;

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        //public static bool GetPrinterProperties(PrinterDescription selectedPrinter, bool showPrintingPreferencesDialog)
        //{
        //    int modeGetSizeOfBuffer = 0;
        //    int modeCopy = 2;
        //    int modeOutBuffer = 14;

        //    IntPtr pointerHDevMode = new IntPtr();
        //    IntPtr pointerDevModeData = new IntPtr();

        //    try
        //    {
        //        PrintTicketConverter printTicketConverter = new PrintTicketConverter(selectedPrinter.FullName, selectedPrinter.ClientPrintSchemaVersion);
        //        IntPtr mainWindowPtr = new WindowInteropHelper(Application.Current.MainWindow).Handle;
        //        PrinterSettings printerSettings = new PrinterSettings();

        //        pointerHDevMode = printerSettings.GetHdevmode(printerSettings.DefaultPageSettings);
        //        IntPtr pointerDevMode = GlobalLock(pointerHDevMode);

        //        int sizeNeeded = DocumentProperties(mainWindowPtr, IntPtr.Zero, selectedPrinter.FullName, IntPtr.Zero, pointerDevMode, modeGetSizeOfBuffer);
        //        pointerDevModeData = Marshal.AllocHGlobal(sizeNeeded);

        //        int result = -1;

        //        if (!showPrintingPreferencesDialog)
        //        {
        //            result = DocumentProperties(mainWindowPtr, IntPtr.Zero, selectedPrinter.FullName, pointerDevModeData, pointerDevMode, modeCopy);
        //        }
        //        else
        //        {
        //            result = DocumentProperties(mainWindowPtr, IntPtr.Zero, selectedPrinter.FullName, pointerDevModeData, pointerDevMode, modeOutBuffer);
        //        }

        //        GlobalUnlock(pointerHDevMode);

        //        if (result == 1)
        //        {
        //            byte[] devMode = new byte[sizeNeeded];
        //            Marshal.Copy(pointerDevModeData, devMode, 0, sizeNeeded);

        //            // set back new printing settings to selected printer.
        //            selectedPrinter.DefaultPrintTicket = printTicketConverter.ConvertDevModeToPrintTicket(devMode);

        //            return true;
        //        }
        //    }
        //    finally
        //    {
        //        GlobalFree(pointerHDevMode);
        //        Marshal.FreeHGlobal(pointerDevModeData);
        //    }

        //    return false;
        //}

        //private void OpenPrinterPropertiesDialog(PrinterSettings printerSettings)
        //{
        //    IntPtr hDevMode = printerSettings.GetHdevmode(printerSettings.DefaultPageSettings);
        //    IntPtr pDevMode = GlobalLock(hDevMode);
        //    int sizeNeeded = DocumentProperties(this.Handle, IntPtr.Zero, printerSettings.PrinterName, pDevMode, ref pDevMode, 0);
        //    IntPtr devModeData = Marshal.AllocHGlobal(sizeNeeded);
        //    DocumentProperties(this.Handle, IntPtr.Zero, printerSettings.PrinterName, devModeData, ref pDevMode, 14);
        //    GlobalUnlock(hDevMode);
        //    printerSettings.SetHdevmode(devModeData);
        //    printerSettings.DefaultPageSettings.SetHdevmode(devModeData);
        //    GlobalFree(hDevMode);
        //    Marshal.FreeHGlobal(devModeData);
        //}

        //private DialogResult EditPrinterSettings(PrinterSettings printerSettings)
        //{
        //    DialogResult myReturnValue = DialogResult.Cancel;
        //    IntPtr hDevMode = printerSettings.GetHdevmode(printerSettings.DefaultPageSettings);
        //    IntPtr pDevMode = GlobalLock(hDevMode);
        //    int sizeNeeded = DocumentProperties(this.Handle, IntPtr.Zero, printerSettings.PrinterName, IntPtr.Zero, pDevMode, 0);
        //    IntPtr devModeData = Marshal.AllocHGlobal(sizeNeeded);
        //    long userChoice = DocumentProperties(this.Handle, IntPtr.Zero, printerSettings.PrinterName, devModeData, pDevMode, DM_IN_BUFFER | DM_PROMPT | DM_OUT_BUFFER);
        //    long IDOK = (long)DialogResult.OK;
        //    if (userChoice == IDOK)
        //    {
        //        myReturnValue = DialogResult.OK;
        //        printerSettings.SetHdevmode(devModeData);
        //        printerSettings.DefaultPageSettings.SetHdevmode(devModeData);
        //    }
        //    GlobalUnlock(hDevMode);
        //    GlobalFree(hDevMode);
        //    Marshal.FreeHGlobal(devModeData);
        //    return myReturnValue;
        //}

        public SettingsForm(MainMenu parameter, Settings parameter2)
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            _menuUtama = parameter;
            _pengaturan = parameter2;
            GetDefaultPrinterToComboBox();
            comboBox2.SelectedIndex = 0;
            GetDefaultLanguage();
        }

        public void SetAllResourcesToDefault()
        {
            this._menuUtama.SetAllControlsResourcesToDefault();
        }

        public void ProperExit()
        {
            foreach (var process in Process.GetProcessesByName("WebDev.WebServer40"))
            {
                process.Kill();
            }
            Application.Exit();
        }

        //load from coffeshop.ini file
        private void SettingsForm_Load(object sender, EventArgs e)
        {
            textBox3.Text = _pengaturan.PgImgLeft.ToString();
            textBox4.Text = _pengaturan.PgImgTop.ToString();
            textBox5.Text = _pengaturan.PgImgD.ToString();

            textBox6.Text = _pengaturan.Fcleft.ToString();
            textBox7.Text = _pengaturan.FcTop.ToString();
            textBox8.Text = _pengaturan.FcWidth.ToString();
            textBox9.Text = _pengaturan.FcHeight.ToString();
            label13.Text = _pengaturan.FcR.ToString();
            label29.Text = _pengaturan.FcG.ToString();
            label27.Text = _pengaturan.FcB.ToString();

            label25.Text = _pengaturan.NetWorkName.ToString();
            label23.Text = _pengaturan.CameraName.ToString();
            label19.Text = _pengaturan.SerialNo.ToString();

            if (_pengaturan.Rotate == 1)
            {
                textBox10.Text = "1";
                this.button1.BackgroundImage = Resources.cup_on_1;
                this.button5.BackgroundImage = Resources.cup_off_2;
                this.button6.BackgroundImage = Resources.cup_off_3;
            }
            else if (_pengaturan.Rotate == 2)
            {
                textBox10.Text = "2";
                this.button5.BackgroundImage = Resources.cup_on_2;
                this.button6.BackgroundImage = Resources.cup_off_3;
                this.button1.BackgroundImage = Resources.cup_off_1;
            }
            else if (_pengaturan.Rotate == 0)
            {
                textBox10.Text = "0";
                this.button6.BackgroundImage = Resources.cup_on_3;
                this.button5.BackgroundImage = Resources.cup_off_2;
                this.button1.BackgroundImage = Resources.cup_off_1;
            }

            //TOP BOTTOM

            if (_pengaturan.Position.Equals("Top"))
            {
                radioButton5.Checked = true;
                pictureBox1.BackgroundImage = Resources.watermark_atas;
            }
            else
            {
                radioButton6.Checked = true;
                pictureBox1.BackgroundImage = Resources.watermark_bawah;
            }

            //WATERMARK

            if (_pengaturan.Watermark.Equals("Template Only"))
            {
                radioButton1.Checked = true;
                pictureBox4.BackgroundImage = Resources.Template;
            }
            else if (_pengaturan.Watermark.Equals("Selfie Only"))
            {
                radioButton2.Checked = true;
                pictureBox4.BackgroundImage = Resources.Selfie;
            }
            else if (_pengaturan.Watermark.Equals("Both"))
            {
                radioButton3.Checked = true;
                pictureBox4.BackgroundImage = Resources.Bersama1;
            }
            else
            {
                radioButton4.Checked = true;
                pictureBox1.BackgroundImage = Resources.MaskTemplate;
                pictureBox4.BackgroundImage = null;
            }

            //STARTUP
            /*
            if (_pengaturan.Startup.Equals("Selfie Only"))
            {
                radioButton13.Checked = true;
            }
            else if (_pengaturan.Startup.Equals("Template Only"))
            {
                radioButton15.Checked = true;
            }
            else
            {
                radioButton14.Checked = true;
            }*/

            //SEPHIA EFFECT

            if (_pengaturan.Sephia.Equals("On"))
            {
                radioButton11.Checked = true;
            }
            else
            {
                radioButton12.Checked = true;
            }

            SwitchLanguage(_pengaturan.Language);

            textBox10.Text = _pengaturan.Rotate.ToString();

            InitComboCategory();
            GetDefaultPrinterToComboBox();
        }

        public void InitComboCategory()
        {
            DirectoryInfo obj = new DirectoryInfo(Directory.GetCurrentDirectory() + @"\lib");
            DirectoryInfo[] folders = obj.GetDirectories();
            comboBox1.DataSource = folders;
            comboBox3.DataSource = folders;
        }

        private void Watermark_Click(object sender, EventArgs e)
        {
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            this._menuUtama.Show();
            this.SetAllResourcesToDefault();
        }

        

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.listView1.Clear();
            imageList1.ImageSize = new Size(100, 100);
            this.imageList1.Images.Clear();
            var select = comboBox1.SelectedItem.ToString();

            var info = new DirectoryInfo(@"lib\" + @select);
            var list = new List<string>();
            for (var i = 0; i < info.GetFiles().Length; i++)
            {
                if ((info.GetFiles()[i].Length <= 0L) ||
                    ((((info.GetFiles()[i].Extension != ".png") && (info.GetFiles()[i].Extension != ".jpg")) &&
                      ((info.GetFiles()[i].Extension != ".gif") && (info.GetFiles()[i].Extension != ".bmp"))) &&
                     (info.GetFiles()[i].Extension != ".jpeg"))) continue;
                var bmp = new FileStream(info.GetFiles()[i].DirectoryName + @"\" + info.GetFiles()[i].Name,
                    FileMode.Open, FileAccess.Read);
                var img = new Bitmap(bmp);
                list.Add(info.GetFiles()[i].Name);
                this.imageList1.Images.Add(img);
                bmp.Close();
            }
            this.listView1.View = View.LargeIcon;
            this.listView1.LargeImageList = this.imageList1;
            this.listView1.BeginUpdate();
            for (var j = 0; j < list.Count; j++)
            {
                var item = new ListViewItem
                {
                    ImageIndex = j,
                    Text = list[j]
                };
                this.listView1.Items.Add(item);
            }
            this.listView1.EndUpdate();
        }

        private void BrowseFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog choofdlog = new OpenFileDialog();
            choofdlog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.jpg;*.bmp;*.png;*.gif)|*.BMP;*.JPG;*.GIF;*.jpg;*.bmp;*.png;*.gif";
            choofdlog.FilterIndex = 1;
            choofdlog.Multiselect = false;

            if (choofdlog.ShowDialog() == DialogResult.OK)
            {
                string sFileName = choofdlog.FileName;
                textBox2.Text = sFileName;
                this.filename = choofdlog.SafeFileName;
            }
        }

        private void AddFile_Click(object sender, EventArgs e)
        {
            var select = comboBox1.SelectedItem.ToString();
            var info = new DirectoryInfo(@"lib\" + @select);
            try
            {
                File.Copy(textBox2.Text, info + "/" + this.filename);
            }
            catch (Exception ex)
            {
                if (ex is IOException)
                {
                    MessageBox.Show("Nama file sudah dipakai.., Ubah nama file terlebih dahulu..");
                }
                else if (ex is ArgumentException)
                {
                    MessageBox.Show("Pilih file terlebih dahulu..");
                }
            }
            comboBox1_SelectedIndexChanged(this, null);
            textBox2.Text = "";
        }

        private void DelFile_Click(object sender, EventArgs e)
        {
            var select = comboBox1.SelectedItem.ToString();
            var info = new DirectoryInfo(@"lib\" + @select);
            try
            {
                var fileTODelete = info + "\\" + this.listView1.SelectedItems[0].Text;
                File.Delete(fileTODelete);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentOutOfRangeException)
                {
                    MessageBox.Show("Pilih file terlebih dahulu..");
                }
                else
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            comboBox1_SelectedIndexChanged(this, null);
        }

        public void SwitchLanguage(String lang)
        {
            if (lang.Equals("English"))
            {
                label1.Text = "Setting";
                tabControl1.TabPages[0].Text = "Cup Size";
                label12.Text = "Cup Orientation";

                tabControl1.TabPages[1].Text = "Watermark";
                label32.Text = "PREVIEW";

                //tabControl1.TabPages[2].Text = "Startup";
                //Selector.Text = "Selection";

                tabControl1.TabPages[2].Text = "Library";
                label38.Text = "PREVIEW";
                label2.Text = "Add/Remove Files";
                button8.Text = "Browse";
                button7.Text = "Add";
                button9.Text = "Delete";
                label3.Text = "Add/Remove Category";
                Tambah.Text = "Add";
                button3.Text = "Delete";


                tabControl1.TabPages[3].Text = "Other";
                label40.Text = "Printer Selection";
                label22.Text = "Printer Name";
                label26.Text = "Network ID";
                label20.Text = "Serial Number";

            }
            else
            {
                label1.Text = "Pengaturan";
                tabControl1.TabPages[0].Text = "Ukuran Cangkir";
                label12.Text = "Orientasi Cangkir";

                tabControl1.TabPages[1].Text = "Tanda Air";
                label32.Text = "Pratinjau";

                //tabControl1.TabPages[2].Text = "Awalan";
                //Selector.Text = "Pilihan";

                tabControl1.TabPages[2].Text = "Kumpulan";
                label38.Text = "Pratinjau";
                label2.Text = "Tambahkan/Hapus Gambar";
                button8.Text = "Cari";
                button7.Text = "Tambah";
                button9.Text = "Hapus";
                label3.Text = "Tambahkan/Hapus Kategori";
                Tambah.Text = "Tambah";
                button3.Text = "Hapus";

                tabControl1.TabPages[3].Text = "Lain-Lain";
                label40.Text = "Pilihan Printer";
                label22.Text = "Nama Printer";
                label26.Text = "Nama Jaringan";
                label20.Text = "Nomor Seri";
            }
            //SWAP STRING
            /*
            String gede = label33.Text;
            String kecil = label34.Text;
            label33.Text = kecil;
            label34.Text = gede;
            */
        }

        /*public void SelectLanguage()
        {
            if (comboBox4.SelectedItem == "Indonesia")
            {

                _pengaturan.Language = "ID";
                //button4.Text = "Ubah Bahasa";

            }
            else
            {
                _pengaturan.Language = "EN";
                //button4.Text = "Change Language";
            }
            SwitchLanguage(_pengaturan.Language);
            this.Refresh();
        }*/
        
        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            
                if (comboBox4.SelectedItem == "Indonesia")
                {

                    _pengaturan.Language = "Indonesia";
                    
                    //button4.Text = "Ubah Bahasa";

                }
                else
                {
                    _pengaturan.Language = "English";
                    
                    //button4.Text = "Change Language";
                }
                SwitchLanguage(_pengaturan.Language);
                this.Refresh();
            
           
            
        }
        public void ApplySettings()
        {
            _pengaturan.PgImgLeft = int.Parse(this.textBox3.Text);
            _pengaturan.PgImgTop = int.Parse(this.textBox4.Text);
            _pengaturan.PgImgD = int.Parse(this.textBox5.Text);
            _pengaturan.Rotate = int.Parse(this.textBox10.Text);
            _pengaturan.Fcleft = int.Parse(this.textBox6.Text);
            _pengaturan.FcTop = int.Parse(this.textBox7.Text);
            _pengaturan.FcWidth = int.Parse(this.textBox8.Text);
            _pengaturan.FcHeight = int.Parse(this.textBox9.Text);
            _pengaturan.Rotate = int.Parse(textBox10.Text);
            _pengaturan.NetWorkName = MainMenu.GetLocalIpAddress();
            //_pengaturan.CameraName = int.Parse(this.textBox1.Text);
            _pengaturan.PrinterName = this.comboBox2.SelectedItem.ToString();
            _pengaturan.Watermark = AddWatermark.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked).Text;
            _pengaturan.Position = Position.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked).Text;
            //_pengaturan.Startup = Selector.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked).Text;
            _pengaturan.Sephia = Sephia.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked).Text;
            _pengaturan.Language = this.comboBox4.SelectedItem.ToString();
            
        }

        public void WriteSettings()
        {
            string contents = "PgImgLeft=" + this.textBox3.Text + Environment.NewLine;
            contents = contents + "PgImgTop=" + this.textBox4.Text + Environment.NewLine;
            contents = contents + "PgImgD=" + this.textBox5.Text + Environment.NewLine;
            contents = contents + "Rotate=" + textBox10.Text + Environment.NewLine;
            contents = contents + "Fcleft=2" + Environment.NewLine;
            contents = contents + "FcTop=" + this.textBox4.Text + Environment.NewLine;
            contents = contents + "FcWidth=0" + Environment.NewLine;
            contents = contents + "FcHeight=" + this.textBox5.Text + Environment.NewLine;
            contents = contents + "FcR=100" + Environment.NewLine;
            contents = contents + "FcG=100" + Environment.NewLine;
            contents = contents + "FcB=100" + Environment.NewLine;
            contents = contents + "NetWorkName=" + MainMenu.GetLocalIpAddress() + Environment.NewLine;
            contents = contents + "CameraName=NULL" + Environment.NewLine;
            contents = contents + "PrinterName=" + this.comboBox2.Text + Environment.NewLine;

            contents = contents + "Watermark=" + _pengaturan.Watermark + Environment.NewLine;
            contents = contents + "Position=" + _pengaturan.Position + Environment.NewLine;
            contents = contents + "Startup=" + _pengaturan.Startup + Environment.NewLine;
            contents = contents + "Sephia=" + _pengaturan.Sephia + Environment.NewLine;

            contents = contents + "SerialNo=" + _pengaturan.SerialNo + Environment.NewLine;
            contents = contents + "Language=" + _pengaturan.Language + Environment.NewLine;
            File.WriteAllText("CoffeeShop.ini", contents);
        }

        public void GetDefaultPrinterToComboBox()
        {
            var num = 0;
            foreach (string str2 in PrinterSettings.InstalledPrinters)
            {
                this.comboBox2.Items.Add(str2);
                if (str2.Equals(_pengaturan.PrinterName))
                {
                    this.comboBox2.SelectedIndex = num;
                }
                num++;
            }
        }

        public void GetDefaultLanguage()
        {
            SwitchLanguage(_pengaturan.Language);
            if (_pengaturan.Language.Equals("English"))
            {
                comboBox4.SelectedIndex = 0;
            } else
            {
                comboBox4.SelectedIndex = 1;
            }
        }

        //Save Button

        private void Save_Click(object sender, EventArgs e)
        {
            ApplySettings();
            WriteSettings();
            //GetDefaultLanguage();
            //SelectLanguage();
            MessageBox.Show("Pengaturan berhasil disimpan!");
            Cancel_Click(this, null);
        }

        private void Save_MouseEnter(object sender, EventArgs e)
        {
            this.Save.BackgroundImage = Resources.SaveClicked;
        }

        private void Save_MouseLeave(object sender, EventArgs e)
        {
            this.Save.BackgroundImage = Resources.Save;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox10.Text = "1";
            this.button1.BackgroundImage = Resources.cup_on_1;
            this.button5.BackgroundImage = Resources.cup_off_2;
            this.button6.BackgroundImage = Resources.cup_off_3;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox10.Text = "2";
            this.button5.BackgroundImage = Resources.cup_on_2;
            this.button6.BackgroundImage = Resources.cup_off_3;
            this.button1.BackgroundImage = Resources.cup_off_1;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox10.Text = "0";
            this.button6.BackgroundImage = Resources.cup_on_3;
            this.button5.BackgroundImage = Resources.cup_off_2;
            this.button1.BackgroundImage = Resources.cup_off_1;
        }

        private void textBox3_Click(object sender, EventArgs e)
        {
            string progFiles = @"C:\Program Files\Common Files\Microsoft Shared\ink";
            string onScreenKeyboardPath = System.IO.Path.Combine(progFiles, "TabTip.exe");
            Process.Start(onScreenKeyboardPath);
        }

        private void textBox4_Click(object sender, EventArgs e)
        {
            string progFiles = @"C:\Program Files\Common Files\Microsoft Shared\ink";
            string onScreenKeyboardPath = System.IO.Path.Combine(progFiles, "TabTip.exe");
            Process.Start(onScreenKeyboardPath);
        }

        private void textBox5_Click(object sender, EventArgs e)
        {
            string progFiles = @"C:\Program Files\Common Files\Microsoft Shared\ink";
            string onScreenKeyboardPath = System.IO.Path.Combine(progFiles, "TabTip.exe");
            Process.Start(onScreenKeyboardPath);
        }

        private void textBox6_Click(object sender, EventArgs e)
        {
            string progFiles = @"C:\Program Files\Common Files\Microsoft Shared\ink";
            string onScreenKeyboardPath = System.IO.Path.Combine(progFiles, "TabTip.exe");
            Process.Start(onScreenKeyboardPath);
        }

        private void textBox7_Click(object sender, EventArgs e)
        {
            string progFiles = @"C:\Program Files\Common Files\Microsoft Shared\ink";
            string onScreenKeyboardPath = System.IO.Path.Combine(progFiles, "TabTip.exe");
            Process.Start(onScreenKeyboardPath);
        }

        private void textBox8_Click(object sender, EventArgs e)
        {
            string progFiles = @"C:\Program Files\Common Files\Microsoft Shared\ink";
            string onScreenKeyboardPath = System.IO.Path.Combine(progFiles, "TabTip.exe");
            Process.Start(onScreenKeyboardPath);
        }

        private void textBox9_Click(object sender, EventArgs e)
        {
            string progFiles = @"C:\Program Files\Common Files\Microsoft Shared\ink";
            string onScreenKeyboardPath = System.IO.Path.Combine(progFiles, "TabTip.exe");
            Process.Start(onScreenKeyboardPath);
        }

        private void textBox10_Click(object sender, EventArgs e)
        {
            string progFiles = @"C:\Program Files\Common Files\Microsoft Shared\ink";
            string onScreenKeyboardPath = System.IO.Path.Combine(progFiles, "TabTip.exe");
            Process.Start(onScreenKeyboardPath);
        }

        private void textBox11_Click(object sender, EventArgs e)
        {
            string progFiles = @"C:\Program Files\Common Files\Microsoft Shared\ink";
            string onScreenKeyboardPath = System.IO.Path.Combine(progFiles, "TabTip.exe");
            Process.Start(onScreenKeyboardPath);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                var ps = new PrinterSettings();
                ps.PrinterName = _pengaturan.PrinterName;
                //MessageBox.Show(ps.PrinterName);
                //OpenPrinterPropertiesDialog(ps);
                //EditPrinterSettings(ps);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (choofdlog.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = choofdlog.FileName;
            }
        }

        private void Add_Click(object sender, EventArgs e)
        {
        }

        private void Remove_Click(object sender, EventArgs e)
        {
        }

        private void Tambah_Click(object sender, EventArgs e)
        {
            if ((!IsNullOrEmpty(textBox11.Text)) || textBox11.Text.Equals("Masukkan nama kategori baru.."))
            {
                var path = Directory.GetCurrentDirectory() + @"\lib\" + textBox11.Text;
                DirectoryInfo dimana = Directory.CreateDirectory(path);
                MessageBox.Show("Folder telah dibuat di " + path);
                InitComboCategory();
                textBox11.Text = "";
            }
            else
            {
                MessageBox.Show("Isi nama kategori..");
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox2.SelectedIndex == null)
            {
                _pengaturan.PrinterName = PrinterSettings.InstalledPrinters[comboBox2.SelectedIndex];
            }
            
        }

        private void radioButton5_Click(object sender, EventArgs e)
        {
            if (!(radioButton4.Checked))
            {
                pictureBox1.BackgroundImage = Resources.watermark_atas;
            }
        }

        private void radioButton6_Click(object sender, EventArgs e)
        {
            if (!(radioButton4.Checked))
            {
                pictureBox1.BackgroundImage = Resources.watermark_bawah;
            }
        }

        private void radioButton4_Click(object sender, EventArgs e)
        {
            pictureBox1.BackgroundImage = Resources.MaskTemplate;
            pictureBox4.BackgroundImage = null;
        }

        private void radioButton1_Click(object sender, EventArgs e)
        {
            pictureBox4.BackgroundImage = Resources.Template;
            if (radioButton5.Checked)
            {
                pictureBox1.BackgroundImage = Resources.watermark_atas;
            }
            else
            {
                pictureBox1.BackgroundImage = Resources.watermark_bawah;
            }
        }

        private void radioButton2_Click(object sender, EventArgs e)
        {
            pictureBox4.BackgroundImage = Resources.Selfie;
            if (radioButton5.Checked)
            {
                pictureBox1.BackgroundImage = Resources.watermark_atas;
            }
            else
            {
                pictureBox1.BackgroundImage = Resources.watermark_bawah;
            }
        }

        private void radioButton3_Click(object sender, EventArgs e)
        {
            pictureBox4.BackgroundImage = Resources.Bersama1;
            if (radioButton5.Checked)
            {
                pictureBox1.BackgroundImage = Resources.watermark_atas;
            }
            else
            {
                pictureBox1.BackgroundImage = Resources.watermark_bawah;
            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var select = comboBox3.SelectedItem.ToString();
            var confirmResult = MessageBox.Show("Yakin untuk menghapus folder " + select + " ?",
                                     "Konfirmasi",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                var mainDir = new DirectoryInfo(@"lib\");
                var info = new DirectoryInfo(@"lib\" + @select);
                try
                {
                    foreach (FileInfo file in info.GetFiles())
                    {
                        file.Delete();
                    }
                    foreach (DirectoryInfo dir in mainDir.GetDirectories())
                    {
                        if (dir.Name.Equals(select))
                        {
                            dir.Delete(true);
                            MessageBox.Show("Sukses dihapus");
                            InitComboCategory();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                comboBox3_SelectedIndexChanged(this, null);
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        

        private void button4_Click(object sender, EventArgs e)
        {
            if (_pengaturan.Language.Equals("EN"))
            {
                _pengaturan.Language = "ID";
                button4.Text = "Ubah Bahasa";
            } else
            {
                _pengaturan.Language = "EN";
                button4.Text = "Change Language";
            }
            
            SwitchLanguage(_pengaturan.Language);
            this.Refresh();
        }

        private void Startup_Click(object sender, EventArgs e)
        {

        }

        private void Cup_Size_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }
        

        //hot size
        private void btn_size_1_Click(object sender, EventArgs e)
        {
            textBox5.Text = "75";
        }

        //cold size
        private void btn_size_2_Click(object sender, EventArgs e)
        {
            textBox5.Text = "90";
        }

        
    }
}