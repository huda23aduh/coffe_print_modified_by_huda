﻿using CoffeeShop.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Windows.Forms;
using Goheer.EXIF;

namespace CoffeeShop.Forms
{
    public partial class TemplateForm : Form
    {
        private static int size = 450;
        private MainMenu _menuUtama;
        private Settings _pengaturan;
        public string filePath = @"lib\";
        public string watermarkPath = @"web\";
        private string lastfilePath = null;
        public Bitmap IMG = new Bitmap(size, size);
        public Bitmap tempImg;
        private Bitmap zoomImg;
        public Bitmap m = new Bitmap(Image.FromFile(@"image/mask_template.png"), size, size);
        public Image mask = Image.FromFile(@"image/mask_template.png");
        private Point mZ = new Point();
        private Point pm = new Point();

        public int tempImgHeight;
        public int tempImgWidth;
        public int tempImgMidX;
        public int tempImgMidY;
        public int tempImgX;
        public int tempImgY;
        private float P;
        public int pBoxD;

        public TemplateForm(MainMenu parameter, Settings parameter2)
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            _menuUtama = parameter;
            _pengaturan = parameter2;
            SetOverlay();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        private void SetOverlay()
        {
            if (this._pengaturan.Watermark.Equals("Both") || this._pengaturan.Watermark.Equals("Template Only"))
            {
                if (this._pengaturan.Position.Equals("Top"))
                {
                    var watermark = this.watermarkPath + "watermark_atas.png";
                    var mark = Image.FromFile(watermark);
                    var m = new Bitmap(mark);
                    pictureBox3.BackgroundImage = m;
                }
                else
                {
                    var watermark = this.watermarkPath + "watermark_bawah.png";
                    var mark = Image.FromFile(watermark);
                    var m = new Bitmap(mark);
                    pictureBox3.BackgroundImage = m;
                }
            }
            else
            {
                pictureBox3.BackgroundImage = null;
            }

            pictureBox3.Parent = pictureBox1;
            pictureBox3.Location = new Point(0, 0);
            pictureBox3.BackColor = Color.Transparent;
            pictureBox3.Size = pictureBox1.Size;
        }

        public void SetAllResourcesToDefault()
        {
            this._menuUtama.SetAllControlsResourcesToDefault();
        }

        public void ProperExit()
        {
            foreach (var process in Process.GetProcessesByName("WebDev.WebServer40"))
            {
                process.Kill();
            }
            Application.Exit();
        }

        private void Mini_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Exit.BackgroundImage = Resources.ExitClicked;
            var prompt = MessageBox.Show("Apakah yakin anda ingin keluar?", "Keluar Program", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Warning);
            if (prompt == DialogResult.OK)
            {
                ProperExit();
            }
            else
            {
                Exit.BackgroundImage = Resources.Exit;
            }
        }

        // Home Button

        private void Home_Click(object sender, EventArgs e)
        {
            this._menuUtama.Show();
            this.SetAllResourcesToDefault();
            this.Dispose();
        }

        private void Home_MouseEnter(object sender, EventArgs e)
        {
            this.Home.BackgroundImage = Resources.Template_HomeClicked;
        }

        private void Home_MouseLeave(object sender, EventArgs e)
        {
            this.Home.BackgroundImage = Resources.Template_Home;
        }

        private void TemplateForm_Load(object sender, EventArgs e)
        {
            InitComboBox();
        }

        public void InitComboBox()
        {
            var path = System.IO.Directory.GetCurrentDirectory() + @"\lib";
            foreach (var dir in Directory.GetDirectories(path))
            {
                var dirName = new DirectoryInfo(dir).Name;
                PilihKategori.Items.Add(dirName);
            }
            PilihKategori.SelectedIndex = 0;
        }

        private void PilihKategori_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.listView1.Clear();
            imageList1.ImageSize = new Size(90, 90);
            this.imageList1.Images.Clear();
            var select = PilihKategori.SelectedItem.ToString();

            var info = new DirectoryInfo(@"lib\" + @select);
            var list = new List<string>();
            for (var i = 0; i < info.GetFiles().Length; i++)
            {
                if ((info.GetFiles()[i].Length <= 0L) ||
                    ((((info.GetFiles()[i].Extension != ".png") && (info.GetFiles()[i].Extension != ".jpg")) &&
                      ((info.GetFiles()[i].Extension != ".gif") && (info.GetFiles()[i].Extension != ".bmp"))) &&
                     (info.GetFiles()[i].Extension != ".jpeg"))) continue;
                var bmp = new FileStream(info.GetFiles()[i].DirectoryName + @"\" + info.GetFiles()[i].Name,
                    FileMode.Open, FileAccess.Read);
                var img = new Bitmap(bmp);
                list.Add(info.GetFiles()[i].Name);
                this.imageList1.Images.Add(img);
                bmp.Close();
            }
            this.listView1.View = View.LargeIcon;
            this.listView1.LargeImageList = this.imageList1;
            this.listView1.BeginUpdate();
            for (var j = 0; j < list.Count; j++)
            {
                var item = new ListViewItem
                {
                    ImageIndex = j,
                    Text = list[j]
                };
                this.listView1.Items.Add(item);
            }
            this.listView1.EndUpdate();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listView1.SelectedItems.Count > 0)
            {
                var filename = this.filePath + PilihKategori.SelectedItem.ToString() + "\\" + this.listView1.SelectedItems[0].Text;

                this.listView1.SelectedItems.Clear();

                if (lastfilePath != filename)
                {
                    var bmp = new FileStream(filename, FileMode.Open, FileAccess.Read);
                    var b = new Bitmap(bmp);
                    RotateImageByEXIF(b);
                    this.addImg(b);
                    this.refImg(this.tempImg);
                    lastfilePath = filename;
                    bmp.Close();
                }
            }
        }

        public Bitmap RotateImageByEXIF(Bitmap bmp)
        {
            var exif = new EXIFextractor(ref bmp, "n");
            if (exif["Orientation"] != null)
            {
                //MessageBox.Show(exif["Orientation"].ToString());
                RotateFlipType flip = OrientationToFlipType(exif["Orientation"].ToString());

                if (flip != RotateFlipType.RotateNoneFlipNone) // don't flip of orientation is correct
                {
                    bmp.RotateFlip(flip);
                    exif.setTag(0x112, "1"); // Optional: reset orientation tag
                    return bmp;
                }
            }
            return bmp;
        }

        private static RotateFlipType OrientationToFlipType(string orientation)
        {
            switch (int.Parse(orientation))
            {
                case 1:
                    return RotateFlipType.RotateNoneFlipNone;
                    break;

                case 2:
                    return RotateFlipType.RotateNoneFlipX;
                    break;

                case 3:
                    return RotateFlipType.Rotate180FlipNone;
                    break;

                case 4:
                    return RotateFlipType.Rotate180FlipX;
                    break;

                case 5:
                    return RotateFlipType.Rotate90FlipX;
                    break;

                case 6:
                    return RotateFlipType.Rotate90FlipNone;
                    break;

                case 7:
                    return RotateFlipType.Rotate270FlipX;
                    break;

                case 8:
                    return RotateFlipType.Rotate270FlipNone;
                    break;

                default:
                    return RotateFlipType.RotateNoneFlipNone;
            }
        }

        private void addImg(Bitmap b)
        {
            var graphics = Graphics.FromImage(this.IMG);
            if (this.tempImg != null)
            {
                graphics.DrawImage(this.tempImg, this.tempImgX, this.tempImgY, this.tempImgWidth, this.tempImgHeight);
            }
            if (b != null)
            {
                this.P = ((float)b.Width) / ((float)b.Height);
                if (this.P > 1f)
                {
                    this.tempImgWidth = (int)(this.P * size);
                    this.tempImgHeight = size;
                    this.tempImgX = (size - this.tempImgWidth) / 2;
                    this.tempImgY = 0;
                }
                else
                {
                    this.tempImgHeight = (int)(size / this.P);
                    this.tempImgWidth = size;
                    this.tempImgY = (size - this.tempImgHeight) / 2;
                    this.tempImgX = 0;
                }
                this.tempImgMidX = this.tempImgX + (this.tempImgWidth / 2);
                this.tempImgMidY = this.tempImgY + (this.tempImgHeight / 2);
                this.tempImg = new Bitmap(b, this.tempImgWidth, this.tempImgHeight);
                this.zoomImg = this.tempImg;
            }
        }

        private void refImg(Bitmap B)
        {
            var image = new Bitmap(this.IMG);
            var graphics = Graphics.FromImage(image);
            if (B != null)
            {
                graphics.DrawImage(B, this.tempImgX, this.tempImgY);
                graphics.DrawImage(this.m, 0, 0);
                var bitmap2 = new Bitmap(image, 482, 478);
                this.pictureBox1.Image = bitmap2;
            }
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Apa anda yakin untuk dihapus?", "", MessageBoxButtons.YesNo) != DialogResult.No)
            {
                var original = Image.FromFile(@"image\background.png");
                this.IMG = new Bitmap(original, size, size);
                this.tempImg = null;
                this.zoomImg = null;
                this.pictureBox1.Image = null;
            }
            lastfilePath = null;
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            this.mZ = e.Location;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button == MouseButtons.Left) && ((((e.X - this.mZ.X) * (e.X - this.mZ.X)) + ((e.Y - this.mZ.Y) * (e.Y - this.mZ.Y))) > 200))
            {
                this.tempImgX += e.X - this.mZ.X;
                this.tempImgY += e.Y - this.mZ.Y;
                this.tempImgMidX = this.tempImgX + (this.tempImgWidth / 2);
                this.tempImgMidY = this.tempImgY + (this.tempImgHeight / 2);
                this.mZ = e.Location;
                this.refImg(this.tempImg);
            }
        }

        private void Zoom_MouseDown(object sender, MouseEventArgs e)
        {
            this.pm = e.Location;
        }

        private void Zoom_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button == MouseButtons.Left) && (this.tempImg != null))
            {
                if (((e.X - this.pm.X) * (e.X - this.pm.X)) < 400)
                {
                    return;
                }
                if ((this.tempImg != null) && (e.Button == MouseButtons.Left))
                {
                    if (this.P > 1f)
                    {
                        this.tempImgWidth += e.X - this.pm.X;
                        this.tempImgHeight = Convert.ToInt32((float)(((float)this.tempImgWidth) / this.P));
                        if (this.tempImgWidth < 50)
                        {
                            this.tempImgWidth = 50;
                            this.tempImgHeight = Convert.ToInt32((float)(((float)this.tempImgWidth) / this.P));
                        }
                    }
                    else
                    {
                        this.tempImgHeight += e.X - this.pm.X;
                        this.tempImgWidth = Convert.ToInt32((float)(this.tempImgHeight * this.P));
                        if (this.tempImgHeight < 50)
                        {
                            this.tempImgHeight = 50;
                            this.tempImgWidth = Convert.ToInt32((float)(this.tempImgHeight * this.P));
                        }
                    }
                }
                this.tempImgX = this.tempImgMidX - (this.tempImgWidth / 2);
                this.tempImgY = this.tempImgMidY - (this.tempImgHeight / 2);
                this.pm.X = e.X;
                this.pm.Y = e.Y;
                this.tempImg = new Bitmap(this.zoomImg, this.tempImgWidth, this.tempImgHeight);
                this.refImg(this.tempImg);
            }
        }

        public void KosongkanPictureBox()
        {
            var original = Image.FromFile(@"image\background.png");
            this.IMG = new Bitmap(original);
            this.tempImg = null;
            this.zoomImg = null;
            this.pictureBox1.Image = null;
        }

        private void Print_Click(object sender, EventArgs e)
        {
            if (this._pengaturan.Watermark.Equals("Both") || this._pengaturan.Watermark.Equals("Selfie Only"))
            {
                if (this._pengaturan.Position.Equals("Top"))
                {
                    var watermark = this.watermarkPath + "watermark_atas.png";
                    var mark = Image.FromFile(watermark);
                    var m = new Bitmap(mark);
                    this.addImg(m);
                    //this.refImg(m);
                }
                else
                {
                    var watermark = this.watermarkPath + "watermark_bawah.png";
                    var mark = Image.FromFile(watermark);
                    var m = new Bitmap(mark);
                    this.addImg(m);
                    //this.refImg(m);
                }
            }
            else
            {
                pictureBox3.BackgroundImage = null;
            }

            pictureBox3.Parent = pictureBox1;
            pictureBox3.Location = new Point(0, 0);
            pictureBox3.BackColor = Color.Transparent;
            pictureBox3.Size = pictureBox1.Size;


            if (MessageBox.Show("Pastikan posisi cangkir sudah di atas dan lampu menyala tidak berkedip.", "Apa anda yakin ingin diprint?", MessageBoxButtons.YesNo) != DialogResult.No)
            {
                if ((_pengaturan.PrinterName == null) || (_pengaturan.PrinterName == ""))
                {
                    MessageBox.Show("Printer tidak ditemukan.");
                }
                else
                {
                    try
                    {
                        this.printDocument1.PrinterSettings.PrinterName = _pengaturan.PrinterName;
                        this.printDocument1.Print();
                        KosongkanPictureBox();
                    }
                    catch
                    {
                        this.printDocument1.PrintController.OnEndPrint(this.printDocument1, new PrintEventArgs());
                    }
                }
            }
        }

        private void printDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            var document = new PrintDocument();
            PaperSize sizePaper = null;
            foreach (PaperSize size2 in document.PrinterSettings.PaperSizes)
            {
                if (size2.PaperName.Equals("A4"))
                {
                    sizePaper = size2;
                }
            }
            document.DefaultPageSettings.Color = false;
            document.DefaultPageSettings.PaperSize = sizePaper;
            this.addImg(null);
            var image = new Bitmap(this.IMG);
            for (var i = 0; i < size; i++)
            {
                for (var k = 0; k < size; k++)
                {
                    if (this.m.GetPixel(i, k).A > 0)
                    {
                        image.SetPixel(i, k, Color.FromArgb(0, 0xff, 0xff, 0xff));
                    }
                }
            }
            if (_pengaturan.Rotate == 1)
            {
                image.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }
            else if (_pengaturan.Rotate == 2)
            {
                image.RotateFlip(RotateFlipType.Rotate270FlipNone);
            }
            var bitmap2 = new Bitmap(20, 20);
            for (var j = 0; j < 20; j++)
            {
                for (var m = 0; m < 20; m++)
                {
                    bitmap2.SetPixel(j, m, Color.FromArgb(0xff, _pengaturan.FcR, _pengaturan.FcG, _pengaturan.FcB));
                }
            }
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            e.Graphics.DrawImage(image, _pengaturan.PgImgLeft - (_pengaturan.PgImgD / 2), _pengaturan.PgImgTop - (_pengaturan.PgImgD / 2), _pengaturan.PgImgD, _pengaturan.PgImgD);
            e.Graphics.DrawImage(bitmap2, _pengaturan.Fcleft, _pengaturan.FcTop, _pengaturan.FcWidth, _pengaturan.FcHeight);
        }

        private void Delete_MouseEnter(object sender, EventArgs e)
        {
            this.Delete.BackgroundImage = Resources.Template_DeleteClicked;
        }

        private void Delete_MouseLeave(object sender, EventArgs e)
        {
            this.Delete.BackgroundImage = Resources.Template_Delete;
        }

        private void Print_MouseEnter(object sender, EventArgs e)
        {
            this.Print.BackgroundImage = Resources.Template_PrintClicked;
        }

        private void Print_MouseLeave(object sender, EventArgs e)
        {
            this.Print.BackgroundImage = Resources.Template_Print;
        }

        private void Mini_MouseEnter(object sender, EventArgs e)
        {
            this.Mini.BackgroundImage = Resources.MiniClicked;
        }

        private void Mini_MouseLeave(object sender, EventArgs e)
        {
            this.Mini.BackgroundImage = Resources.Mini;
        }

        private void Exit_MouseEnter(object sender, EventArgs e)
        {
            this.Exit.BackgroundImage = Resources.ExitClicked;
        }

        private void Exit_MouseLeave(object sender, EventArgs e)
        {
            this.Exit.BackgroundImage = Resources.Exit;
        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}