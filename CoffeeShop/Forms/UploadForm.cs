using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Web.UI;
using System.Windows.Forms;
using CoffeeShop.Properties;
using ThoughtWorks.QRCode.Codec;
using static System.Environment;
using Goheer.EXIF;

namespace CoffeeShop.Forms
{
    public partial class UploadForm : Form
    {
        private MainMenu _menuUtama;
        private Settings _pengaturan;
        public Bitmap IMG = new Bitmap(500, 500);
        public Bitmap tempImg;
        private Bitmap zoomImg;
        public string filePath = @"lib\";
        public string watermarkPath = @"web\";
        public Bitmap m = new Bitmap(Image.FromFile(@"image/mask.png"), 500, 500);
        public Image mask = Image.FromFile(@"image/mask.png");
        private Point mZ;
        private Point pm;
        public int tempImgHeight;
        public int tempImgWidth;
        public int tempImgMidX;
        public int tempImgMidY;
        public int tempImgX;
        public int tempImgY;
        private float P;
        public int pBoxD;

        [DllImport("user32.dll")]
        public static extern Boolean GetLastInputInfo(ref tagLASTINPUTINFO plii);

        public struct tagLASTINPUTINFO
        {
            public uint cbSize;
            public Int32 dwTime;
        }

        public UploadForm(MainMenu parameter, Settings parameter2)
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            _menuUtama = parameter;
            _pengaturan = parameter2;
            SetOverlay();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        public void SetAllResourcesToDefault()
        {
            this._menuUtama.SetAllControlsResourcesToDefault();
        }

        private void SetOverlay()
        {
            if (this._pengaturan.Watermark.Equals("Both") || this._pengaturan.Watermark.Equals("Selfie Only"))
            {
                if (this._pengaturan.Position.Equals("Top"))
                {
                    var watermark = this.watermarkPath + "watermark_atas.png";
                    var mark = Image.FromFile(watermark);
                    var m = new Bitmap(mark);
                    pictureBox3.BackgroundImage = m;
                }
                else
                {
                    var watermark = this.watermarkPath + "watermark_bawah.png";
                    var mark = Image.FromFile(watermark);
                    var m = new Bitmap(mark);
                    pictureBox3.BackgroundImage = m;
                }
            }
            else
            {
                pictureBox3.BackgroundImage = null;
            }

            pictureBox3.Parent = pictureBox1;
            pictureBox3.Location = new Point(0, 0);
            pictureBox3.BackColor = Color.Transparent;
            pictureBox3.Size = pictureBox1.Size;
        }

        private void Home_Click(object sender, EventArgs e)
        {
            this.Hide();
            this._menuUtama.Show();
            this.SetAllResourcesToDefault();
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Apa anda yakin untuk dihapus?", "", MessageBoxButtons.YesNo) != DialogResult.No)
            {
                var original = Image.FromFile(@"image\background.png");
                this.IMG = new Bitmap(original);
                this.tempImg = null;
                this.zoomImg = null;
                this.pictureBox1.Image = null;
            }
            this.delFile();
            this.TimerUploadToShow.Enabled = true;
        }

        private void Print_Click(object sender, EventArgs e)
        {
            if (this._pengaturan.Watermark.Equals("Both") || this._pengaturan.Watermark.Equals("Selfie Only"))
            {
                if (this._pengaturan.Position.Equals("Top"))
                {
                    var watermark = this.watermarkPath + "watermark_atas.png";
                    var mark = Image.FromFile(watermark);
                    var m = new Bitmap(mark);
                    this.addImg(m);
                    this.refImg(this.tempImg);
                }
                else
                {
                    var watermark = this.watermarkPath + "watermark_bawah.png";
                    var mark = Image.FromFile(watermark);
                    var m = new Bitmap(mark);
                    this.addImg(m);
                    this.refImg(this.tempImg);
                }
            }
            //show confirmation box
            if (MessageBox.Show("Pastikan posisi cangkir sudah di atas dan lampu menyala tidak berkedip.",
                    "Apa anda yakin ingin diprint?", MessageBoxButtons.YesNo) != DialogResult.No)
            {
                //condiition if printer not found
                if ((_pengaturan.PrinterName == null) || (_pengaturan.PrinterName == ""))
                {
                    MessageBox.Show("Printer tidak ditemukan.");
                }
                else
                {
                    try
                    {
                        this.printDocument1.PrinterSettings.PrinterName = _pengaturan.PrinterName;
                        this.printDocument1.Print();
                        KosongkanPictureBox();
                    }
                    catch
                    {
                        this.printDocument1.PrintController.OnEndPrint(this.printDocument1, new PrintEventArgs());
                    }
                }
            }
        }

        private void UploadForm_Load(object sender, EventArgs e)
        {
            var content = "http://" + _pengaturan.NetWorkName + ":1000/web/Default.aspx";
            var original = new QRCodeEncoder
            {
                QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE,
                QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.L,
                QRCodeVersion = 0,
                QRCodeScale = 8
            }.Encode(content, Encoding.UTF8);
            var bitmap2 = new Bitmap(original, 218, 211);
            this.pictureBox2.Image = bitmap2;
        }

        private void TimerUploadToShow_Tick(object sender, EventArgs e)
        {
            var info = new DirectoryInfo(@"web\update\");
            try
            {
                if (info.GetFiles().Length != 0)
                {
                    var path = info.GetFiles()[0].DirectoryName + @"\" + info.GetFiles()[0].Name;
                    var bmp = new FileStream(path, FileMode.Open, FileAccess.Read);
                    var b = new Bitmap(bmp);
                    RotateImageByEXIF(b);
                    this.addImg(b);
                    this.refImg(this.tempImg);
                    bmp.Dispose();
                    TimerUploadToShow.Enabled = false;
                }
            }
            catch (Exception)
            {
            }
        }

        private void addImg(Bitmap b)
        {
            var graphics = Graphics.FromImage(this.IMG);
            if (this.tempImg != null)
            {
                graphics.DrawImage(this.tempImg, this.tempImgX, this.tempImgY, this.tempImgWidth, this.tempImgHeight);
            }
            if (b != null)
            {
                this.P = ((float)b.Width) / ((float)b.Height);
                if (this.P > 1f)
                {
                    this.tempImgWidth = (int)(this.P * 500f);
                    this.tempImgHeight = 500;
                    this.tempImgX = (500 - this.tempImgWidth) / 2;
                    this.tempImgY = 0;
                }
                else
                {
                    this.tempImgHeight = (int)(500f / this.P);
                    this.tempImgWidth = 500;
                    this.tempImgY = (500 - this.tempImgHeight) / 2;
                    this.tempImgX = 0;
                }
                this.tempImgMidX = this.tempImgX + (this.tempImgWidth / 2);
                this.tempImgMidY = this.tempImgY + (this.tempImgHeight / 2);
                this.tempImg = new Bitmap(b, this.tempImgWidth, this.tempImgHeight);
                this.zoomImg = this.tempImg;
            }
        }

        private void refImg(Bitmap B)
        {
            var image = new Bitmap(this.IMG);
            var graphics = Graphics.FromImage(image);
            if (B != null)
            {
                graphics.DrawImage(B, this.tempImgX, this.tempImgY);
                graphics.DrawImage(this.m, 0, 0);
                var bitmap2 = new Bitmap(image, 482, 478);
                this.pictureBox1.Image = bitmap2;
            }
        }

        private void delFile()
        {
            if (File.Exists(@"web\update\upd.bmp"))
            {
                File.Delete(@"web\update\upd.bmp");
            }
            else if (File.Exists(@"web\update\upd.x-png"))
            {
                File.Delete(@"web\update\upd.x-png");
            }
            else if (File.Exists(@"web\update\upd.pjpeg"))
            {
                File.Delete(@"web\update\upd.pjpeg");
            }
            else if (File.Exists(@"web\update\upd.gif"))
            {
                File.Delete(@"web\update\upd.gif");
            }
            else if (File.Exists(@"web\update\upd.png"))
            {
                File.Delete(@"web\update\upd.png");
            }
            else if (File.Exists(@"web\update\upd.jpeg"))
            {
                File.Delete(@"web\update\upd.jpeg");
            }
            else if (File.Exists(@"web\update\upd.jpg"))
            {
                File.Delete(@"web\update\upd.jpg");
            }
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            this.mZ = e.Location;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button == MouseButtons.Left) && ((((e.X - this.mZ.X) * (e.X - this.mZ.X)) +
                                                     ((e.Y - this.mZ.Y) * (e.Y - this.mZ.Y))) > 200))
            {
                this.tempImgX += e.X - this.mZ.X;
                this.tempImgY += e.Y - this.mZ.Y;
                this.tempImgMidX = this.tempImgX + (this.tempImgWidth / 2);
                this.tempImgMidY = this.tempImgY + (this.tempImgHeight / 2);
                this.mZ = e.Location;
                this.refImg(this.tempImg);
            }
        }

        private void Zoom_MouseDown(object sender, MouseEventArgs e)
        {
            this.pm = e.Location;
        }

        private void Zoom_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button == MouseButtons.Left) && (this.tempImg != null))
            {
                if (((e.X - this.pm.X) * (e.X - this.pm.X)) < 400)
                {
                    return;
                }
                if ((this.tempImg != null) && (e.Button == MouseButtons.Left))
                {
                    if (this.P > 1f)
                    {
                        this.tempImgWidth += e.X - this.pm.X;
                        this.tempImgHeight = Convert.ToInt32((float)(((float)this.tempImgWidth) / this.P));
                        if (this.tempImgWidth < 50)
                        {
                            this.tempImgWidth = 50;
                            this.tempImgHeight = Convert.ToInt32((float)(((float)this.tempImgWidth) / this.P));
                        }
                    }
                    else
                    {
                        this.tempImgHeight += e.X - this.pm.X;
                        this.tempImgWidth = Convert.ToInt32((float)(this.tempImgHeight * this.P));
                        if (this.tempImgHeight < 50)
                        {
                            this.tempImgHeight = 50;
                            this.tempImgWidth = Convert.ToInt32((float)(this.tempImgHeight * this.P));
                        }
                    }
                }
                this.tempImgX = this.tempImgMidX - (this.tempImgWidth / 2);
                this.tempImgY = this.tempImgMidY - (this.tempImgHeight / 2);
                this.pm.X = e.X;
                this.pm.Y = e.Y;
                this.tempImg = new Bitmap(this.zoomImg, this.tempImgWidth, this.tempImgHeight);
                this.refImg(this.tempImg);
            }
        }

        private void Mini_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Exit.BackgroundImage = Resources.ExitClicked;
            var prompt = MessageBox.Show("Apakah yakin anda ingin keluar?", "Keluar Program",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Warning);
            if (prompt == DialogResult.OK)
            {
                ProperExit();
                delFile();
            }
            else
            {
                Exit.BackgroundImage = Resources.Exit;
            }
        }

        public void ProperExit()
        {
            foreach (var process in Process.GetProcessesByName("WebDev.WebServer40"))
            {
                process.Kill();
            }
            Application.Exit();
        }

        private void printDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            var document = new PrintDocument();
            PaperSize size = null;
            foreach (PaperSize size2 in document.PrinterSettings.PaperSizes)
            {
                if (size2.PaperName.Equals("A4"))
                {
                    size = size2;
                }
            }
            document.DefaultPageSettings.PaperSize = size;
            this.addImg(null);
            var image = new Bitmap(this.IMG);
            for (var i = 0; i < 500; i++)
            {
                for (var k = 0; k < 500; k++)
                {
                    if (this.m.GetPixel(i, k).A > 0)
                    {
                        image.SetPixel(i, k, Color.FromArgb(0, 0xff, 0xff, 0xff));
                    }
                }
            }
            if (_pengaturan.Rotate == 1)
            {
                image.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }
            else if (_pengaturan.Rotate == 2)
            {
                image.RotateFlip(RotateFlipType.Rotate270FlipNone);
            }
            var bitmap2 = new Bitmap(20, 20);
            for (var j = 0; j < 20; j++)
            {
                for (var m = 0; m < 20; m++)
                {
                    bitmap2.SetPixel(j, m, Color.FromArgb(0xff, _pengaturan.FcR, _pengaturan.FcG, _pengaturan.FcB));
                }
            }
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            e.Graphics.DrawImage(image, _pengaturan.PgImgLeft - (_pengaturan.PgImgD / 2),
                _pengaturan.PgImgTop - (_pengaturan.PgImgD / 2), _pengaturan.PgImgD, _pengaturan.PgImgD);
            e.Graphics.DrawImage(bitmap2, _pengaturan.Fcleft, _pengaturan.FcTop, _pengaturan.FcWidth,
                _pengaturan.FcHeight);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            tagLASTINPUTINFO LastInput = new tagLASTINPUTINFO();
            Int32 IdleTime;
            LastInput.cbSize = (uint)Marshal.SizeOf(LastInput);
            LastInput.dwTime = 0;

            if (GetLastInputInfo(ref LastInput))
            {
                IdleTime = TickCount - LastInput.dwTime;
                label1.Text = IdleTime + "ms";
                if (IdleTime > 180000)
                {
                    Home_Click(this, null);

                    delFile();
                    //Dispose uploaded image
                }
            }
        }

        private void Home_MouseEnter(object sender, EventArgs e)
        {
            this.Home.BackgroundImage = Resources.Selfie_HomeClicked;
        }

        private void Home_MouseLeave(object sender, EventArgs e)
        {
            this.Home.BackgroundImage = Resources.Selfie_Home;
        }

        private void Delete_MouseEnter(object sender, EventArgs e)
        {
            this.Delete.BackgroundImage = Resources.Selfie_DeleteClicked;
        }

        private void Delete_MouseLeave(object sender, EventArgs e)
        {
            this.Delete.BackgroundImage = Resources.Selfie_Delete;
        }

        private void Print_MouseEnter(object sender, EventArgs e)
        {
            this.Print.BackgroundImage = Resources.Selfie_PrintClicked;
        }

        private void Print_MouseLeave(object sender, EventArgs e)
        {
            this.Print.BackgroundImage = Resources.Selfie_Print;
        }

        private void Mini_MouseEnter(object sender, EventArgs e)
        {
            this.Mini.BackgroundImage = Resources.MiniClicked;
        }

        private void Mini_MouseLeave(object sender, EventArgs e)
        {
            this.Mini.BackgroundImage = Resources.Mini;
        }

        private void Exit_MouseEnter(object sender, EventArgs e)
        {
            this.Exit.BackgroundImage = Resources.ExitClicked;
        }

        private void Exit_MouseLeave(object sender, EventArgs e)
        {
            this.Exit.BackgroundImage = Resources.Exit;
        }

        private void Insert_Click(object sender, EventArgs e)
        {
            OpenFileDialog choofdlog = new OpenFileDialog();
            choofdlog.Filter =
                "Image Files(*.BMP;*.JPG;*.GIF;*.jpg;*.bmp;*.png;*.gif)|*.BMP;*.JPG;*.GIF;*.jpg;*.bmp;*.png;*.gif";
            choofdlog.FilterIndex = 1;
            choofdlog.Multiselect = false;

            if (choofdlog.ShowDialog() == DialogResult.OK)
            {
                var bmp = new FileStream(choofdlog.FileName, FileMode.Open, FileAccess.Read);
                var b = new Bitmap(bmp);
                RotateImageByEXIF(b);
                this.addImg(b);
                this.refImg(this.tempImg);
                bmp.Close();
            }
        }

        public Bitmap RotateImageByEXIF(Bitmap bmp)
        {
            var exif = new Goheer.EXIF.EXIFextractor(ref bmp, "n");
            foreach (Pair item in exif)
            {
                if (item.First.Equals("Orientation"))
                {
                    RotateFlipType flip = OrientationToFlipType(item.Second.ToString());

                    if (flip != RotateFlipType.RotateNoneFlipNone) // don't flip of orientation is correct
                    {
                        bmp.RotateFlip(flip);
                        return bmp;
                    }
                }
            }
            return bmp;
        }

        private static RotateFlipType OrientationToFlipType(string orientation)
        {
            switch (int.Parse(orientation))
            {
                case 1:
                    return RotateFlipType.RotateNoneFlipNone;
                    break;

                case 2:
                    return RotateFlipType.RotateNoneFlipX;
                    break;

                case 3:
                    return RotateFlipType.Rotate180FlipNone;
                    break;

                case 4:
                    return RotateFlipType.Rotate180FlipX;
                    break;

                case 5:
                    return RotateFlipType.Rotate90FlipX;
                    break;

                case 6:
                    return RotateFlipType.Rotate90FlipNone;
                    break;

                case 7:
                    return RotateFlipType.Rotate270FlipX;
                    break;

                case 8:
                    return RotateFlipType.Rotate270FlipNone;
                    break;

                default:
                    return RotateFlipType.RotateNoneFlipNone;
            }
        }

        public void KosongkanPictureBox()
        {
            var original = Image.FromFile(@"image\background.png");
            this.IMG = new Bitmap(original);
            this.tempImg = null;
            this.zoomImg = null;
            this.pictureBox1.Image = null;
        }

        private void Insert_MouseEnter(object sender, EventArgs e)
        {
            this.Insert.BackgroundImage = Resources.Selfie_InsertClicked;
        }

        private void Insert_MouseLeave(object sender, EventArgs e)
        {
            this.Insert.BackgroundImage = Resources.Selfie_Insert;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btn_cold_Click(object sender, EventArgs e)
        {
            _pengaturan.PgImgD = 90;
            btn_cold.ForeColor = Color.Red;
            btn_hot.ForeColor = Color.Transparent;
        }

        private void btn_hot_Click(object sender, EventArgs e)
        {
            _pengaturan.PgImgD = 75;
            btn_hot.ForeColor = Color.Red;
            btn_cold.ForeColor = Color.Transparent;
        }
    }
}