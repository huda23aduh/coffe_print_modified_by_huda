﻿namespace CoffeeShop.Forms
{
    partial class NewFormUpload
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewFormUpload));
            this.Logo = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_home = new System.Windows.Forms.Button();
            this.Mini = new System.Windows.Forms.Button();
            this.Exit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_refresh = new System.Windows.Forms.Button();
            this.bntStart = new System.Windows.Forms.Button();
            this.Home = new System.Windows.Forms.Button();
            this.Print = new System.Windows.Forms.Button();
            this.Zoom = new System.Windows.Forms.Button();
            this.Delete = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.imgVideo = new System.Windows.Forms.PictureBox();
            this.bntStop = new System.Windows.Forms.Button();
            this.bntSave = new System.Windows.Forms.Button();
            this.bntCapture = new System.Windows.Forms.Button();
            this.imgCapture = new System.Windows.Forms.PictureBox();
            this.btn_hot = new System.Windows.Forms.Button();
            this.btn_cold = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.Title = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVideo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCapture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // Logo
            // 
            this.Logo.BackColor = System.Drawing.Color.Transparent;
            this.Logo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Logo.BackgroundImage")));
            this.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Logo.Location = new System.Drawing.Point(34, 32);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(147, 170);
            this.Logo.TabIndex = 27;
            this.Logo.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.btn_home);
            this.panel3.Controls.Add(this.Mini);
            this.panel3.Controls.Add(this.Exit);
            this.panel3.Location = new System.Drawing.Point(988, 12);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(258, 157);
            this.panel3.TabIndex = 29;
            // 
            // btn_home
            // 
            this.btn_home.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_home.BackgroundImage = global::CoffeeShop.Properties.Resources.Back;
            this.btn_home.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_home.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_home.FlatAppearance.BorderSize = 0;
            this.btn_home.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btn_home.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_home.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_home.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_home.Location = new System.Drawing.Point(42, 32);
            this.btn_home.Name = "btn_home";
            this.btn_home.Size = new System.Drawing.Size(59, 67);
            this.btn_home.TabIndex = 6;
            this.btn_home.UseVisualStyleBackColor = true;
            this.btn_home.Click += new System.EventHandler(this.btn_home_Click);
            // 
            // Mini
            // 
            this.Mini.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Mini.BackgroundImage = global::CoffeeShop.Properties.Resources.Mini;
            this.Mini.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Mini.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Mini.FlatAppearance.BorderSize = 0;
            this.Mini.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Mini.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Mini.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Mini.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Mini.Location = new System.Drawing.Point(98, 17);
            this.Mini.Name = "Mini";
            this.Mini.Size = new System.Drawing.Size(76, 82);
            this.Mini.TabIndex = 5;
            this.Mini.UseVisualStyleBackColor = true;
            this.Mini.Click += new System.EventHandler(this.Mini_Click);
            // 
            // Exit
            // 
            this.Exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Exit.BackgroundImage = global::CoffeeShop.Properties.Resources.Exit;
            this.Exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exit.FlatAppearance.BorderSize = 0;
            this.Exit.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Exit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Exit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exit.Location = new System.Drawing.Point(172, 17);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(82, 83);
            this.Exit.TabIndex = 4;
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.btn_refresh);
            this.panel1.Controls.Add(this.bntStart);
            this.panel1.Controls.Add(this.Home);
            this.panel1.Controls.Add(this.Print);
            this.panel1.Controls.Add(this.Zoom);
            this.panel1.Controls.Add(this.Delete);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 619);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1264, 130);
            this.panel1.TabIndex = 36;
            // 
            // btn_refresh
            // 
            this.btn_refresh.BackgroundImage = global::CoffeeShop.Properties.Resources.Upload;
            this.btn_refresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_refresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_refresh.Location = new System.Drawing.Point(149, 30);
            this.btn_refresh.Name = "btn_refresh";
            this.btn_refresh.Size = new System.Drawing.Size(75, 69);
            this.btn_refresh.TabIndex = 40;
            this.btn_refresh.UseVisualStyleBackColor = true;
            this.btn_refresh.Click += new System.EventHandler(this.btn_refresh_Click);
            // 
            // bntStart
            // 
            this.bntStart.BackgroundImage = global::CoffeeShop.Properties.Resources.Camera;
            this.bntStart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bntStart.Location = new System.Drawing.Point(68, 30);
            this.bntStart.Name = "bntStart";
            this.bntStart.Size = new System.Drawing.Size(75, 69);
            this.bntStart.TabIndex = 45;
            this.bntStart.UseVisualStyleBackColor = true;
            this.bntStart.Click += new System.EventHandler(this.bntStart_Click);
            // 
            // Home
            // 
            this.Home.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Home.BackColor = System.Drawing.Color.Transparent;
            this.Home.BackgroundImage = global::CoffeeShop.Properties.Resources.Selfie_Home;
            this.Home.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Home.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Home.FlatAppearance.BorderSize = 0;
            this.Home.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Home.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Home.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Home.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Home.ForeColor = System.Drawing.Color.Transparent;
            this.Home.Location = new System.Drawing.Point(93, 19);
            this.Home.Margin = new System.Windows.Forms.Padding(25, 3, 3, 3);
            this.Home.Name = "Home";
            this.Home.Size = new System.Drawing.Size(88, 90);
            this.Home.TabIndex = 24;
            this.Home.UseVisualStyleBackColor = false;
            this.Home.Visible = false;
            this.Home.Click += new System.EventHandler(this.Home_Click);
            // 
            // Print
            // 
            this.Print.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Print.BackColor = System.Drawing.Color.Transparent;
            this.Print.BackgroundImage = global::CoffeeShop.Properties.Resources.Selfie_Print;
            this.Print.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Print.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Print.FlatAppearance.BorderSize = 0;
            this.Print.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Print.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Print.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Print.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Print.Location = new System.Drawing.Point(1106, 21);
            this.Print.Margin = new System.Windows.Forms.Padding(25, 3, 3, 3);
            this.Print.Name = "Print";
            this.Print.Size = new System.Drawing.Size(88, 90);
            this.Print.TabIndex = 22;
            this.Print.UseVisualStyleBackColor = false;
            this.Print.Click += new System.EventHandler(this.Print_Click);
            // 
            // Zoom
            // 
            this.Zoom.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Zoom.BackColor = System.Drawing.Color.Transparent;
            this.Zoom.BackgroundImage = global::CoffeeShop.Properties.Resources.Zoom_Move_Selfie_mac;
            this.Zoom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Zoom.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Zoom.FlatAppearance.BorderSize = 0;
            this.Zoom.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Zoom.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Zoom.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Zoom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Zoom.ForeColor = System.Drawing.Color.Transparent;
            this.Zoom.Location = new System.Drawing.Point(414, 36);
            this.Zoom.Name = "Zoom";
            this.Zoom.Size = new System.Drawing.Size(394, 57);
            this.Zoom.TabIndex = 21;
            this.Zoom.UseVisualStyleBackColor = false;
            // 
            // Delete
            // 
            this.Delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Delete.BackColor = System.Drawing.Color.Transparent;
            this.Delete.BackgroundImage = global::CoffeeShop.Properties.Resources.Selfie_Delete;
            this.Delete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Delete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Delete.FlatAppearance.BorderSize = 0;
            this.Delete.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.Delete.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Delete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Delete.Location = new System.Drawing.Point(980, 23);
            this.Delete.Margin = new System.Windows.Forms.Padding(25, 3, 3, 3);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(88, 90);
            this.Delete.TabIndex = 23;
            this.Delete.UseVisualStyleBackColor = false;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(653, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 41;
            this.label1.Text = "label1";
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Mistral", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(600, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 23);
            this.label2.TabIndex = 42;
            this.label2.Text = "NEW FORM UPLOAD";
            this.label2.Visible = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BackgroundImage = global::CoffeeShop.Properties.Resources.watermark_atas;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Enabled = false;
            this.pictureBox3.Location = new System.Drawing.Point(437, 100);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(484, 473);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 44;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Location = new System.Drawing.Point(437, 90);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(484, 477);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 43;
            this.pictureBox1.TabStop = false;
            // 
            // imgVideo
            // 
            this.imgVideo.BackColor = System.Drawing.Color.Transparent;
            this.imgVideo.Location = new System.Drawing.Point(34, 237);
            this.imgVideo.Name = "imgVideo";
            this.imgVideo.Size = new System.Drawing.Size(230, 230);
            this.imgVideo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgVideo.TabIndex = 46;
            this.imgVideo.TabStop = false;
            // 
            // bntStop
            // 
            this.bntStop.BackColor = System.Drawing.Color.White;
            this.bntStop.BackgroundImage = global::CoffeeShop.Properties.Resources.camera_off_logo;
            this.bntStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bntStop.Location = new System.Drawing.Point(231, 112);
            this.bntStop.Name = "bntStop";
            this.bntStop.Size = new System.Drawing.Size(75, 69);
            this.bntStop.TabIndex = 47;
            this.bntStop.UseVisualStyleBackColor = false;
            this.bntStop.Visible = false;
            this.bntStop.Click += new System.EventHandler(this.bntStop_Click);
            // 
            // bntSave
            // 
            this.bntSave.Location = new System.Drawing.Point(115, 208);
            this.bntSave.Name = "bntSave";
            this.bntSave.Size = new System.Drawing.Size(95, 23);
            this.bntSave.TabIndex = 48;
            this.bntSave.Text = "bntSave";
            this.bntSave.UseVisualStyleBackColor = true;
            this.bntSave.Visible = false;
            this.bntSave.Click += new System.EventHandler(this.bntSave_Click);
            // 
            // bntCapture
            // 
            this.bntCapture.BackColor = System.Drawing.Color.White;
            this.bntCapture.BackgroundImage = global::CoffeeShop.Properties.Resources.Take_Pict;
            this.bntCapture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bntCapture.Location = new System.Drawing.Point(93, 543);
            this.bntCapture.Name = "bntCapture";
            this.bntCapture.Size = new System.Drawing.Size(110, 47);
            this.bntCapture.TabIndex = 49;
            this.bntCapture.UseVisualStyleBackColor = false;
            this.bntCapture.Click += new System.EventHandler(this.bntCapture_Click);
            // 
            // imgCapture
            // 
            this.imgCapture.BackColor = System.Drawing.Color.Transparent;
            this.imgCapture.Location = new System.Drawing.Point(34, 237);
            this.imgCapture.Name = "imgCapture";
            this.imgCapture.Size = new System.Drawing.Size(230, 230);
            this.imgCapture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgCapture.TabIndex = 50;
            this.imgCapture.TabStop = false;
            // 
            // btn_hot
            // 
            this.btn_hot.BackColor = System.Drawing.Color.White;
            this.btn_hot.BackgroundImage = global::CoffeeShop.Properties.Resources.Hot;
            this.btn_hot.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_hot.Location = new System.Drawing.Point(1086, 300);
            this.btn_hot.Name = "btn_hot";
            this.btn_hot.Size = new System.Drawing.Size(95, 86);
            this.btn_hot.TabIndex = 52;
            this.btn_hot.UseVisualStyleBackColor = false;
            this.btn_hot.Click += new System.EventHandler(this.btn_hot_Click);
            // 
            // btn_cold
            // 
            this.btn_cold.BackgroundImage = global::CoffeeShop.Properties.Resources.Cold;
            this.btn_cold.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cold.Location = new System.Drawing.Point(1086, 408);
            this.btn_cold.Name = "btn_cold";
            this.btn_cold.Size = new System.Drawing.Size(95, 86);
            this.btn_cold.TabIndex = 51;
            this.btn_cold.UseVisualStyleBackColor = true;
            this.btn_cold.Click += new System.EventHandler(this.btn_cold_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(209, 78);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 53;
            this.comboBox1.Visible = false;
            // 
            // Title
            // 
            this.Title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Title.BackColor = System.Drawing.Color.Transparent;
            this.Title.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Title.BackgroundImage")));
            this.Title.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Title.Location = new System.Drawing.Point(625, 21);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(150, 50);
            this.Title.TabIndex = 54;
            this.Title.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Location = new System.Drawing.Point(950, 187);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(292, 242);
            this.pictureBox2.TabIndex = 55;
            this.pictureBox2.TabStop = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(255, 22);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 37;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            // 
            // NewFormUpload
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::CoffeeShop.Properties.Resources.Background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1264, 749);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.btn_hot);
            this.Controls.Add(this.btn_cold);
            this.Controls.Add(this.imgCapture);
            this.Controls.Add(this.bntCapture);
            this.Controls.Add(this.bntSave);
            this.Controls.Add(this.bntStop);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.Logo);
            this.Controls.Add(this.imgVideo);
            this.Controls.Add(this.pictureBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "NewFormUpload";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NewFormUpload";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.NewFormUpload_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVideo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCapture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Logo;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button Mini;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button Home;
        private System.Windows.Forms.Button Print;
        private System.Windows.Forms.Button Zoom;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.Button btn_refresh;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button bntStart;
        private System.Windows.Forms.PictureBox imgVideo;
        private System.Windows.Forms.Button bntStop;
        private System.Windows.Forms.Button bntSave;
        private System.Windows.Forms.Button bntCapture;
        private System.Windows.Forms.PictureBox imgCapture;
        private System.Windows.Forms.Button btn_hot;
        private System.Windows.Forms.Button btn_cold;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.PictureBox Title;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btn_home;
        private System.Windows.Forms.Button button2;
    }
}