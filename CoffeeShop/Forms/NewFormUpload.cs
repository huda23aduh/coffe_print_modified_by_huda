﻿using CoffeeShop.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Windows.Forms;

//aforge lib
using AForge.Video;
using AForge.Video.DirectShow;

namespace CoffeeShop.Forms
{
    public partial class NewFormUpload : Form
    {
        string the_image_path = @"c:\coffee_print\the_image.jpg";
        string the_coffee_print_directory = @"c:\coffee_print";

        string the_image_from_webcam_path = @"c:\coffee_print_from_webcam\the_image.jpg";
        string the_coffee_print_from_webcam_directory = @"c:\coffee_print_from_webcam";

        private static int size = 450;
        private MainMenu _menuUtama;
        private Settings _pengaturan;
        public string filePath = @"lib\";
        public string watermarkPath = @"web\";
        private string lastfilePath = null;
        public Bitmap IMG = new Bitmap(size, size);
        public Bitmap tempImg;
        private Bitmap zoomImg;
        public Bitmap m = new Bitmap(Image.FromFile(@"image/mask_template.png"), size, size);
        public Image mask = Image.FromFile(@"image/mask_template.png");
        private Point mZ = new Point();
        private Point pm = new Point();

        public int tempImgHeight;
        public int tempImgWidth;
        public int tempImgMidX;
        public int tempImgMidY;
        public int tempImgX;
        public int tempImgY;
        private float P;
        public int pBoxD;

        private Point dragPoint = Point.Empty;
        private bool dragging = false;

        FilterInfoCollection webcam;
        VideoCaptureDevice cam;
        Bitmap bitmap;


        public NewFormUpload(MainMenu parameter, Settings parameter2)
        {
            InitializeComponent();
            {
                webcam = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                foreach (FilterInfo VideoCaptureDevice in webcam)
                {
                    comboBox1.Items.Add(VideoCaptureDevice.Name);
                }
                comboBox1.SelectedIndex = 0;
            }

            //newformupload
            this.Load += new System.EventHandler(this.NewFormUpload_Load);

            //picturebox1
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);

            //zoom
            this.Zoom.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Zoom_MouseDown);
            this.Zoom.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Zoom_MouseMove);

            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);

            this.DoubleBuffered = true;
            _menuUtama = parameter;
            _pengaturan = parameter2;
            SetOverlay();

            System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();
            path.AddEllipse(0, 0, imgVideo.Width, imgVideo.Height);

            imgVideo.Region = new Region(path);
            imgCapture.Region = new Region(path);
            
        }


        private void NewFormUpload_Load(object sender, EventArgs e)
        {
            
        }

        private void bntStart_Click(object sender, EventArgs e)
        {
            imgVideo.BringToFront();

            cam = new VideoCaptureDevice(webcam[comboBox1.SelectedIndex].MonikerString);
            cam.NewFrame += new NewFrameEventHandler(FinalVideo_NewFrame);
            cam.Start();

        }
        void FinalVideo_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap video = (Bitmap)eventArgs.Frame.Clone();
            imgVideo.Image = video;

        }

        private void bntStop_Click(object sender, EventArgs e)
        {
            cam.Stop();
        }

        private void bntContinue_Click(object sender, EventArgs e)
        {
            
        }

        private void bntCapture_Click(object sender, EventArgs e)
        {
            //execute button delete first
            var original = Image.FromFile(@"image\background.png");
            this.IMG = new Bitmap(original, size, size);
            this.tempImg = null;
            this.zoomImg = null;
            this.pictureBox1.Image = null;

            lastfilePath = null;


            try
            {
                cam.Stop();
                imgVideo.Image.Save(this.the_image_from_webcam_path, ImageFormat.Jpeg);
            }
            catch (Exception ex)
            {

            }
            
            

            imgCapture.BringToFront();
            imgVideo.SendToBack();

            // check folder
            if (Directory.Exists(this.the_coffee_print_from_webcam_directory))
            {
                label1.Text = "directory exists";

                //check image file
                try
                {

                    FileStream stream = new FileStream(the_image_from_webcam_path, FileMode.Open, FileAccess.Read);
                    var b = new Bitmap(stream);
                    RotateImageByEXIF(b);
                    this.addImg(b);
                    this.refImg(this.tempImg);
                    stream.Close();


                    label1.Text = "Sucess";
                }
                catch (Exception ex)
                {
                    pictureBox1.Refresh();
                    Console.WriteLine(ex.ToString());
                    label1.Text = ex.ToString();
                }
            }

            else
            {
                label1.Text = "directory not exists";
                try
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(this.the_coffee_print_from_webcam_directory);
                    label1.Text = "The directory was created successfully at {0}.";
                }
                catch (Exception ex)
                {
                    label1.Text = "The process failed: " + ex;
                }
                finally { }
            }

            

            imgVideo.SendToBack();
            imgCapture.BringToFront();
        }

        private void bntSave_Click(object sender, EventArgs e)
        {
            Helper.SaveImageCapture(imgCapture.Image);
        }

        private void bntVideoFormat_Click(object sender, EventArgs e)
        {
            //
        }

        private void bntVideoSource_Click(object sender, EventArgs e)
        {
           
        }

        // Exit Button

        private void Exit_Click(object sender, EventArgs e)
        {
            Exit.BackgroundImage = Resources.ExitClicked;
            DialogResult prompt = MessageBox.Show("Apakah yakin anda ingin keluar?", "Keluar Program", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Warning);
            if (prompt == DialogResult.OK)
            {
                ProperExit();
            }
            else
            {
                Exit.BackgroundImage = Resources.Exit;
            }
        }

        // Minimize Button

        private void Mini_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        public void ProperExit()
        {
            foreach (var process in Process.GetProcessesByName("WebDev.WebServer40"))
            {
                process.Kill();
            }
            Application.Exit();
        }

        private void addImg(Bitmap b)
        {
            var graphics = Graphics.FromImage(this.IMG);
            if (this.tempImg != null)
            {
                graphics.DrawImage(this.tempImg, this.tempImgX, this.tempImgY, this.tempImgWidth, this.tempImgHeight);
            }
            if (b != null)
            {
                this.P = ((float)b.Width) / ((float)b.Height);
                if (this.P > 1f)
                {
                    this.tempImgWidth = (int)(this.P * 500f);
                    this.tempImgHeight = 500;
                    this.tempImgX = (500 - this.tempImgWidth) / 2;
                    this.tempImgY = 0;
                }
                else
                {
                    this.tempImgHeight = (int)(500f / this.P);
                    this.tempImgWidth = 500;
                    this.tempImgY = (500 - this.tempImgHeight) / 2;
                    this.tempImgX = 0;
                }
                this.tempImgMidX = this.tempImgX + (this.tempImgWidth / 2);
                this.tempImgMidY = this.tempImgY + (this.tempImgHeight / 2);
                this.tempImg = new Bitmap(b, this.tempImgWidth, this.tempImgHeight);
                this.zoomImg = this.tempImg;
            }
        }

        private void refImg(Bitmap B)
        {
            var image = new Bitmap(this.IMG);
            var graphics = Graphics.FromImage(image);
            if (B != null)
            {
                graphics.DrawImage(B, this.tempImgX, this.tempImgY);
                graphics.DrawImage(this.m, 0, 0);
                var bitmap2 = new Bitmap(image, 482, 478);
                this.pictureBox1.Image = bitmap2;
            }
        }

        private void Home_Click(object sender, EventArgs e)
        {
            this.Hide();
            this._menuUtama.Show();
            this.SetAllResourcesToDefault();
        }

        public void SetAllResourcesToDefault()
        {
            this._menuUtama.SetAllControlsResourcesToDefault();
        }

        private void btn_refresh_Click(object sender, EventArgs e)
        {
            //execute button delete first
            var original = Image.FromFile(@"image\background.png");
            this.IMG = new Bitmap(original, size, size);
            this.tempImg = null;
            this.zoomImg = null;
            this.pictureBox1.Image = null;

            lastfilePath = null;

            //end of execute button delete first

            // check folder
            if (Directory.Exists(this.the_coffee_print_directory))
                label1.Text = "directory exists";

            else
            {
                label1.Text = "directory not exists";
                try
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(this.the_coffee_print_directory);
                    label1.Text = "The directory was created successfully at {0}.";
                }
                catch (Exception ex)
                {
                    label1.Text = "The process failed: " + ex;
                }
                finally { }
            }

            //check file image
            if (File.Exists(this.the_image_path))
            {
                label1.Text = "file image exists";
                try
                {
                    FileStream stream = new FileStream(this.the_image_path, FileMode.Open, FileAccess.Read);
                    var b = new Bitmap(stream);
                    RotateImageByEXIF(b);
                    this.addImg(b);
                    this.refImg(this.tempImg);
                    stream.Close();
                    

                    label1.Text = "Sucess";
                }
                catch (Exception ex)
                {
                    pictureBox1.Refresh();
                    Console.WriteLine(ex.ToString());
                    label1.Text = ex.ToString();
                }



            }
            else
                label1.Text = "file image not exists";
            
        }
       
        public Bitmap RotateImageByEXIF(Bitmap bmp)
        {
            var exif = new Goheer.EXIF.EXIFextractor(ref bmp, "n");
            foreach (Pair item in exif)
            {
                if (item.First.Equals("Orientation"))
                {
                    RotateFlipType flip = OrientationToFlipType(item.Second.ToString());

                    if (flip != RotateFlipType.RotateNoneFlipNone) // don't flip of orientation is correct
                    {
                        bmp.RotateFlip(flip);
                        return bmp;
                    }
                }
            }
            return bmp;
        }

        private static RotateFlipType OrientationToFlipType(string orientation)
        {
            switch (int.Parse(orientation))
            {
                case 1:
                    return RotateFlipType.RotateNoneFlipNone;
                    break;

                case 2:
                    return RotateFlipType.RotateNoneFlipX;
                    break;

                case 3:
                    return RotateFlipType.Rotate180FlipNone;
                    break;

                case 4:
                    return RotateFlipType.Rotate180FlipX;
                    break;

                case 5:
                    return RotateFlipType.Rotate90FlipX;
                    break;

                case 6:
                    return RotateFlipType.Rotate90FlipNone;
                    break;

                case 7:
                    return RotateFlipType.Rotate270FlipX;
                    break;

                case 8:
                    return RotateFlipType.Rotate270FlipNone;
                    break;

                default:
                    return RotateFlipType.RotateNoneFlipNone;
            }
        }

        private void Print_Click(object sender, EventArgs e)
        {
            //if (this._pengaturan.Watermark.Equals("Both") || this._pengaturan.Watermark.Equals("Selfie Only"))
            //{
            //    if (this._pengaturan.Position.Equals("Top"))
            //    {
            //        var watermark = this.watermarkPath + "watermark_atas.png";
            //        var mark = Image.FromFile(watermark);
            //        var m = new Bitmap(mark);
            //        this.addImg(m);
            //        this.refImg(this.tempImg);
            //    }
            //    else
            //    {
            //        var watermark = this.watermarkPath + "watermark_bawah.png";
            //        var mark = Image.FromFile(watermark);
            //        var m = new Bitmap(mark);
            //        this.addImg(m);
            //        this.refImg(this.tempImg);
            //    }
            //}
            //show confirmation box
            if (MessageBox.Show("Pastikan posisi cangkir sudah di atas dan lampu menyala tidak berkedip.",
                    "Apa anda yakin ingin diprint?", MessageBoxButtons.YesNo) != DialogResult.No)
            {
                //condiition if printer not found
                if ((_pengaturan.PrinterName == null) || (_pengaturan.PrinterName == ""))
                {
                    MessageBox.Show("Printer tidak ditemukan.");
                }
                else
                {
                    try
                    {
                        this.printDocument1.PrinterSettings.PrinterName = _pengaturan.PrinterName;
                        this.printDocument1.Print();
                        KosongkanPictureBox();
                    }
                    catch
                    {
                        this.printDocument1.PrintController.OnEndPrint(this.printDocument1, new PrintEventArgs());
                    }
                }
            }
            //this action
            

        }

        private void printDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            var document = new PrintDocument();
            PaperSize sizePaper = null;
            foreach (PaperSize size2 in document.PrinterSettings.PaperSizes)
            {
                if (size2.PaperName.Equals("A4"))
                {
                    sizePaper = size2;
                }
            }
            document.DefaultPageSettings.Color = false;
            document.DefaultPageSettings.PaperSize = sizePaper;
            this.addImg(null);
            var image = new Bitmap(this.IMG);
            for (var i = 0; i < size; i++)
            {
                for (var k = 0; k < size; k++)
                {
                    if (this.m.GetPixel(i, k).A > 0)
                    {
                        image.SetPixel(i, k, Color.FromArgb(0, 0xff, 0xff, 0xff));
                    }
                }
            }
            if (_pengaturan.Rotate == 1)
            {
                image.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }
            else if (_pengaturan.Rotate == 2)
            {
                image.RotateFlip(RotateFlipType.Rotate270FlipNone);
            }
            var bitmap2 = new Bitmap(20, 20);
            for (var j = 0; j < 20; j++)
            {
                for (var m = 0; m < 20; m++)
                {
                    bitmap2.SetPixel(j, m, Color.FromArgb(0xff, _pengaturan.FcR, _pengaturan.FcG, _pengaturan.FcB));
                }
            }
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            e.Graphics.DrawImage(image, _pengaturan.PgImgLeft - (_pengaturan.PgImgD / 2), _pengaturan.PgImgTop - (_pengaturan.PgImgD / 2), _pengaturan.PgImgD, _pengaturan.PgImgD);
            e.Graphics.DrawImage(bitmap2, _pengaturan.Fcleft, _pengaturan.FcTop, _pengaturan.FcWidth, _pengaturan.FcHeight);
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            this.mZ = e.Location;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            int aa = 999;
            if ((e.Button == MouseButtons.Left) && ((((e.X - this.mZ.X) * (e.X - this.mZ.X)) +
                                                     ((e.Y - this.mZ.Y) * (e.Y - this.mZ.Y))) > 200))
            {
                this.tempImgX += e.X - this.mZ.X;
                this.tempImgY += e.Y - this.mZ.Y;
                this.tempImgMidX = this.tempImgX + (this.tempImgWidth / 2);
                this.tempImgMidY = this.tempImgY + (this.tempImgHeight / 2);
                this.mZ = e.Location;
                this.refImg(this.tempImg);
            }
        }

        private void Zoom_MouseDown(object sender, MouseEventArgs e)
        {
            this.pm = e.Location;
        }

        private void Zoom_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button == MouseButtons.Left) && (this.tempImg != null))
            {
                if (((e.X - this.pm.X) * (e.X - this.pm.X)) < 400)
                {
                    return;
                }
                if ((this.tempImg != null) && (e.Button == MouseButtons.Left))
                {
                    if (this.P > 1f)
                    {
                        this.tempImgWidth += e.X - this.pm.X;
                        this.tempImgHeight = Convert.ToInt32((float)(((float)this.tempImgWidth) / this.P));
                        if (this.tempImgWidth < 50)
                        {
                            this.tempImgWidth = 50;
                            this.tempImgHeight = Convert.ToInt32((float)(((float)this.tempImgWidth) / this.P));
                        }
                    }
                    else
                    {
                        this.tempImgHeight += e.X - this.pm.X;
                        this.tempImgWidth = Convert.ToInt32((float)(this.tempImgHeight * this.P));
                        if (this.tempImgHeight < 50)
                        {
                            this.tempImgHeight = 50;
                            this.tempImgWidth = Convert.ToInt32((float)(this.tempImgHeight * this.P));
                        }
                    }
                }
                this.tempImgX = this.tempImgMidX - (this.tempImgWidth / 2);
                this.tempImgY = this.tempImgMidY - (this.tempImgHeight / 2);
                this.pm.X = e.X;
                this.pm.Y = e.Y;
                this.tempImg = new Bitmap(this.zoomImg, this.tempImgWidth, this.tempImgHeight);
                this.refImg(this.tempImg);
            }
        }

        public void KosongkanPictureBox()
        {
            var original = Image.FromFile(@"image\background.png");
            this.IMG = new Bitmap(original);



            this.tempImg = null;
            this.zoomImg = null;
            this.pictureBox1.Image = null;
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Apa anda yakin untuk dihapus?", "", MessageBoxButtons.YesNo) != DialogResult.No)
            {
                var original = Image.FromFile(@"image\background.png");
                this.IMG = new Bitmap(original, size, size);
                this.tempImg = null;
                this.zoomImg = null;
                this.pictureBox1.Image = null;
            }
            lastfilePath = null;
        }

        private void SetOverlay()
        {
            if (this._pengaturan.Watermark.Equals("Both") || this._pengaturan.Watermark.Equals("Template Only"))
            {
                if (this._pengaturan.Position.Equals("Top"))
                {
                    var watermark = this.watermarkPath + "watermark_atas.png";
                    var mark = Image.FromFile(watermark);
                    var m = new Bitmap(mark);
                    pictureBox3.BackgroundImage = m;
                }
                else
                {
                    var watermark = this.watermarkPath + "watermark_bawah.png";
                    var mark = Image.FromFile(watermark);
                    var m = new Bitmap(mark);
                    pictureBox3.BackgroundImage = m;
                }
            }
            else
            {
                pictureBox3.BackgroundImage = null;
            }

            pictureBox3.Parent = pictureBox1;
            pictureBox3.Location = new Point(0, 0);
            pictureBox3.BackColor = Color.Transparent;
            pictureBox3.Size = pictureBox1.Size;
        }

        private void btn_camera_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = imgVideo.Image;
        }

        private void btn_hot_Click(object sender, EventArgs e)
        {
            btn_hot.BackgroundImage = Resources.Hot_klik;
            btn_cold.BackgroundImage = Resources.Cold;

            _pengaturan.PgImgD = 75;
            btn_hot.ForeColor = Color.Red;
            btn_cold.ForeColor = Color.Transparent;
        }

        private void btn_cold_Click(object sender, EventArgs e)
        {
            btn_cold.BackgroundImage = Resources.Cold_klik;
            btn_hot.BackgroundImage = Resources.Hot;

            _pengaturan.PgImgD = 90;
            btn_hot.ForeColor = Color.Red;
            btn_cold.ForeColor = Color.Transparent;
        }

        private void NewFormUpload_Load_1(object sender, EventArgs e)
        {
            // check coffee_print folder
            if (Directory.Exists(this.the_coffee_print_directory))
                label1.Text = "directory exists";

            else
            {
                label1.Text = "directory not exists";
                try
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(this.the_coffee_print_directory);
                    label1.Text = "The directory was created successfully at {0}.";
                }
                catch (Exception ex)
                {
                    label1.Text = "The process failed: " + ex;
                }
                finally { }
            }

            // check web_camera_coffee_print folder
            if (Directory.Exists(this.the_coffee_print_from_webcam_directory))
                label1.Text = "directory exists";

            else
            {
                label1.Text = "directory not exists";
                try
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(this.the_coffee_print_from_webcam_directory);
                    label1.Text = "The directory was created successfully at {0}.";
                }
                catch (Exception ex)
                {
                    label1.Text = "The process failed: " + ex;
                }
                finally { }
            }
        }

        private void btn_home_Click(object sender, EventArgs e)
        {
            this.Hide();
            this._menuUtama.Show();
            this.SetAllResourcesToDefault();
        }
    }
}
