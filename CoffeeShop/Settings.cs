﻿namespace CoffeeShop
{
    public class Settings
    {
        public int PgImgLeft { get; set; }
        public int PgImgTop { get; set; }
        public int PgImgD { get; set; }

        public int Rotate { get; set; }

        public int Fcleft { get; set; }
        public int FcTop { get; set; }
        public int FcWidth { get; set; }
        public int FcHeight { get; set; }

        public int FcR { get; set; }
        public int FcG { get; set; }
        public int FcB { get; set; }

        public string NetWorkName { get; set; }
        public string CameraName { get; set; }
        public string PrinterName { get; set; }

        public string Watermark { get; set; }
        public string Position { get; set; }
        public string Startup { get; set; }
        public string Sephia { get; set; }

        public string SerialNo { get; set; }
        public string Language { get; set; }
    }
}