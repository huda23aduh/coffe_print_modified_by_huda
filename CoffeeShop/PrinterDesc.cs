﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Printing;

namespace CoffeeShop
{
    public class PrinterDescription
    {
        public string FullName { get; set; }
        public int ClientPrintSchemaVersion { get; set; }
        public PrintTicket DefaultPrintTicket { get; set; }
    }
}