﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeShop
{
    public class PrinterData
    {
        public PrinterData()
        {
            Duplex = 1;
            source = 1;
            Orientation = 1;
            Size = 1;
        }

        public int Duplex { get; set; }
        public int source { get; set; }
        public int Orientation { get; set; }
        public int Size { get; set; }
    }
}